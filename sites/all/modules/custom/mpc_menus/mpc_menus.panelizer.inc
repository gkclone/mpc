<?php
/**
 * @file
 * mpc_menus.panelizer.inc
 */

/**
 * Implements hook_panelizer_defaults().
 */
function mpc_menus_panelizer_defaults() {
  $export = array();

  $panelizer = new stdClass();
  $panelizer->disabled = FALSE; /* Edit this to true to make a default panelizer disabled initially */
  $panelizer->api_version = 1;
  $panelizer->name = 'gk_menu:gk_menu_type__default:mpc_default';
  $panelizer->title = 'MPC: Default';
  $panelizer->panelizer_type = 'gk_menu';
  $panelizer->panelizer_key = 'gk_menu_type__default';
  $panelizer->no_blocks = FALSE;
  $panelizer->css_id = '';
  $panelizer->css = '';
  $panelizer->pipeline = 'ipe';
  $panelizer->contexts = array();
  $panelizer->relationships = array();
  $panelizer->access = array();
  $panelizer->view_mode = 'page_manager';
  $panelizer->css_class = '';
  $panelizer->title_element = 'H2';
  $panelizer->link_to_entity = TRUE;
  $panelizer->extra = array();
  $display = new panels_display();
  $display->layout = 'mpc_menus_default';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'primary' => NULL,
      'secondary' => NULL,
      'top' => NULL,
      'bottom' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '%gk_menu:title';
  $display->uuid = 'bf462690-288a-439c-b431-0602accfada3';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-dbeabb17-49cb-497f-ab4c-354e545f961f';
    $pane->panel = 'primary';
    $pane->type = 'entity_view';
    $pane->subtype = 'gk_menu';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'view_mode' => 'full',
      'context' => 'panelizer',
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'dbeabb17-49cb-497f-ab4c-354e545f961f';
    $display->content['new-dbeabb17-49cb-497f-ab4c-354e545f961f'] = $pane;
    $display->panels['primary'][0] = 'new-dbeabb17-49cb-497f-ab4c-354e545f961f';
    $pane = new stdClass();
    $pane->pid = 'new-689c677b-5831-4b0c-bcad-180cd703aaad';
    $pane->panel = 'secondary';
    $pane->type = 'navigation';
    $pane->subtype = 'navigation';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => '',
      'show_outline' => 0,
      'filter_by_current_location' => 1,
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '689c677b-5831-4b0c-bcad-180cd703aaad';
    $display->content['new-689c677b-5831-4b0c-bcad-180cd703aaad'] = $pane;
    $display->panels['secondary'][0] = 'new-689c677b-5831-4b0c-bcad-180cd703aaad';
    $pane = new stdClass();
    $pane->pid = 'new-ec6118d8-3c8a-46c3-b082-dd391ddd3f74';
    $pane->panel = 'top';
    $pane->type = 'pane_content_header';
    $pane->subtype = 'pane_content_header';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array();
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'ec6118d8-3c8a-46c3-b082-dd391ddd3f74';
    $display->content['new-ec6118d8-3c8a-46c3-b082-dd391ddd3f74'] = $pane;
    $display->panels['top'][0] = 'new-ec6118d8-3c8a-46c3-b082-dd391ddd3f74';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = '0';
  $panelizer->display = $display;
  $export['gk_menu:gk_menu_type__default:mpc_default'] = $panelizer;

  return $export;
}
