<?php

// Plugin definition
$plugin = array(
  'title' => t('MPC: Menus default'),
  'category' => t('2 Columns'),
  'icon' => 'mpc_menus_default.png',
  'theme' => 'minima_page_layout',
  'regions' => array(
    'top' => t('Top'),
    'primary' => t('Primary'),
    'secondary' => t('Secondary'),
    'bottom' => t('Bottom'),
  ),
);

/**
 * Implements minima_layout_LAYOUT_NAME().
 */
function minima_layout_mpc_menus_default($variables) {
  return array(
    'top' => array(
      '#markup' => $variables['content']['top'],
    ),
    'main' => array(
      '#theme_wrappers' => array('minima_grid', 'minima_container'),
      '#container_element' => 'main',
      '#container_attributes' => array(
        'class' => array('Container--constrained', 'Container--main'),
      ),
      'primary' => array(
        '#theme_wrappers' => array('minima_grid_cell'),
        '#grid_cell_attributes' => array(
          'class' => array('u-xl-size3of4', 'u-ie-size3of4', 'u-xl-push1of4')
        ),
        '#markup' => $variables['content']['primary'],
      ),
      'secondary' => array(
        '#theme_wrappers' => array('minima_grid_cell'),
        '#grid_cell_attributes' => array(
          'class' => array('u-xl-size1of4', 'u-ie-size1of4', 'u-xl-pull3of4')
        ),
        '#markup' => $variables['content']['secondary'],
      ),
    ),
    'bottom' => array(
      '#markup' => $variables['content']['bottom'],
    ),
  );
}
