<?php
/**
 * @file
 * mpc_events.panelizer.inc
 */

/**
 * Implements hook_panelizer_defaults().
 */
function mpc_events_panelizer_defaults() {
  $export = array();

  $panelizer = new stdClass();
  $panelizer->disabled = FALSE; /* Edit this to true to make a default panelizer disabled initially */
  $panelizer->api_version = 1;
  $panelizer->name = 'node:event:mpc_default';
  $panelizer->title = 'MPC: Default';
  $panelizer->panelizer_type = 'node';
  $panelizer->panelizer_key = 'event';
  $panelizer->no_blocks = FALSE;
  $panelizer->css_id = '';
  $panelizer->css = '';
  $panelizer->pipeline = 'ipe';
  $panelizer->contexts = array();
  $panelizer->relationships = array();
  $panelizer->access = array();
  $panelizer->view_mode = 'page_manager';
  $panelizer->css_class = '';
  $panelizer->title_element = 'H2';
  $panelizer->link_to_entity = TRUE;
  $panelizer->extra = array();
  $display = new panels_display();
  $display->layout = '1_column';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'primary' => NULL,
      'secondary' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '%node:title';
  $display->uuid = '19166602-490b-4cd3-bbd9-6bd32f9c39b6';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-73ee5ede-4277-45e3-a57c-8717df958036';
    $pane->panel = 'primary';
    $pane->type = 'pane_content_header';
    $pane->subtype = 'pane_content_header';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array();
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '73ee5ede-4277-45e3-a57c-8717df958036';
    $display->content['new-73ee5ede-4277-45e3-a57c-8717df958036'] = $pane;
    $display->panels['primary'][0] = 'new-73ee5ede-4277-45e3-a57c-8717df958036';
    $pane = new stdClass();
    $pane->pid = 'new-b339bda8-6bf9-4e1e-bb46-772180f3c73c';
    $pane->panel = 'primary';
    $pane->type = 'entity_view';
    $pane->subtype = 'node';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'view_mode' => 'full',
      'context' => 'panelizer',
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = 'b339bda8-6bf9-4e1e-bb46-772180f3c73c';
    $display->content['new-b339bda8-6bf9-4e1e-bb46-772180f3c73c'] = $pane;
    $display->panels['primary'][1] = 'new-b339bda8-6bf9-4e1e-bb46-772180f3c73c';
    $pane = new stdClass();
    $pane->pid = 'new-f03e995a-1aac-41e9-8341-ffa3253b3b0b';
    $pane->panel = 'primary';
    $pane->type = 'gk_members_signup_cta_form';
    $pane->subtype = 'gk_members_signup_cta_form';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 2;
    $pane->locks = array();
    $pane->uuid = 'f03e995a-1aac-41e9-8341-ffa3253b3b0b';
    $display->content['new-f03e995a-1aac-41e9-8341-ffa3253b3b0b'] = $pane;
    $display->panels['primary'][2] = 'new-f03e995a-1aac-41e9-8341-ffa3253b3b0b';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = '0';
  $panelizer->display = $display;
  $export['node:event:mpc_default'] = $panelizer;

  return $export;
}
