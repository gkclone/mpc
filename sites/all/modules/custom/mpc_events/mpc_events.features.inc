<?php
/**
 * @file
 * mpc_events.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function mpc_events_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "panelizer" && $api == "panelizer") {
    return array("version" => "1");
  }
}
