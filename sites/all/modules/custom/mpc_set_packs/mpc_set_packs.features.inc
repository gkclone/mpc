<?php
/**
 * @file
 * mpc_set_packs.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function mpc_set_packs_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function mpc_set_packs_node_info() {
  $items = array(
    'set_pack' => array(
      'name' => t('Set pack'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
