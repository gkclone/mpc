<?php

$plugin = array(
  'title' => 'MPC: Set packs list',
  'category' => 'Metropolitan Pub Company',
  'single' => TRUE,
);

function mpc_set_packs_set_packs_list_content_type_render($subtype, $conf, $args, $context) {
  if ($set_packs = mpc_set_packs_get_items()) {
    $content = array(
      'intro' => array(
        '#theme_wrappers' => array('container'),
        '#attributes' => array(
          'class' => array('SetPacksList-intro'),
        ),
        '#markup' => $conf['intro']['value'],
      ),
      'set_packs_list' => array(
        '#theme_wrappers' => array('minima_grid'),
        '#grid_attributes' => array(
          'class' => array('Grid--space'),
        ),
        'nodes' => array(
          'content' => array(),
        ),
      ),
    );

    foreach ($set_packs as $set_pack) {
      $content['set_packs_list']['nodes']['content'][$set_pack->nid] = node_view($set_pack, 'teaser');
    }

    return (object) array(
      'title' => '',
      'content' => $content,
    );
  }
}

function mpc_set_packs_set_packs_list_content_type_edit_form($form, &$form_state) {
  $conf = $form_state['conf'];

  // Optional introduction text.
  $intro = isset($conf['intro']) ? $conf['intro'] : NULL;

  $form['intro'] = array(
    '#type' => 'text_format',
    '#title' => 'Intro',
    '#default_value' => $intro ? $intro['value'] : '',
    '#format' => $intro ? $intro['format'] : NULL,
  );

  return $form;
}

function mpc_set_packs_set_packs_list_content_type_edit_form_submit($form, &$form_state) {
  form_state_values_clean($form_state);
  $form_state['conf'] = $form_state['values'];
  $form_state['conf']['sections'] = array_filter($form_state['conf']['sections']);
}
