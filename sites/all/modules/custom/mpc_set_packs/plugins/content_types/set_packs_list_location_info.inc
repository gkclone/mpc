<?php

$plugin = array(
  'title' => 'MPC: Set packs list/Location info',
  'category' => 'Metropolitan Pub Company',
  'single' => TRUE,
);

function mpc_set_packs_set_packs_list_location_info_content_type_render($subtype, $conf, $args, $context) {
  $content = array(
    'set_packs_list_location_info' => array(
      '#theme_wrappers' => array('minima_grid'),
      '#grid_attributes' => array(
        'class' => array('Grid--space'),
      ),
    ),
  );

  // Set packs
  ctools_plugin_load_function('ctools', 'content_types', 'set_packs_list', 'render callback');

  if ($set_packs = mpc_set_packs_set_packs_list_content_type_render($subtype, $conf, $args, $context)) {
    $content['set_packs_list_location_info']['nodes'] = array(
      '#theme_wrappers' => array('minima_grid_cell'),
      '#grid_cell_attributes' => array(
        'class' => array('u-xl-size2of3', 'u-lg-size2of3', 'u-ie-size2of3')
      ),
      'content' => $set_packs->content['set_packs_list']['nodes']['content'],
    );
  }

  // Location info.
  ctools_plugin_load_function('ctools', 'content_types', 'gk_locations_info', 'render callback');

  if ($location_info = gk_locations_gk_locations_info_content_type_render($subtype, $conf, $args, $context)) {
    $content['set_packs_list_location_info']['location_info'] = array(
      '#theme_wrappers' => array('container', 'minima_grid_cell'),
      '#attributes' => array(
        'class' => array('Box', 'Box--locationInfo'),
      ),
      '#grid_cell_attributes' => array(
        'class' => array('u-xl-size1of3', 'u-lg-size1of3', 'u-ie-size1of3')
      ),
      'box' => array(
        '#theme_wrappers' => array('container'),
        '#attributes' => array(
          'class' => array('Box-content'),
        ),
        'content' => (array) $location_info,
      ),
    );
  }

  return (object) array(
    'title' => '',
    'content' => $content,
  );
}

function mpc_set_packs_set_packs_list_location_info_content_type_edit_form($form, &$form_state) {
  form_load_include($form_state, 'inc', 'gk_locations', 'plugins/content_types/gk_locations_info');
  $form += gk_locations_gk_locations_info_content_type_edit_form($form, $form_state);

  return $form;
}

function mpc_set_packs_set_packs_list_location_info_content_type_edit_form_submit($form, &$form_state) {
  form_state_values_clean($form_state);
  $form_state['conf'] = $form_state['values'];
  $form_state['conf']['sections'] = array_filter($form_state['conf']['sections']);
}
