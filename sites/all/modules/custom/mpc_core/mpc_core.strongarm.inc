<?php
/**
 * @file
 * mpc_core.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function mpc_core_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'domain_menu_block_content_menu';
  $strongarm->value = array(
    0 => 'main_menu',
    1 => 'footer_menu',
  );
  $export['domain_menu_block_content_menu'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'domain_menu_block_parent_menu';
  $strongarm->value = 'main_menu';
  $export['domain_menu_block_parent_menu'] = $strongarm;

  return $export;
}
