<?php

// Plugin definition
$plugin = array(
  'title' => t('MPC: Site default'),
  'category' => t('Site'),
  'icon' => 'mpc_core_site_default.png',
  'theme' => 'minima_site_layout',
  'regions' => array(
    'header' => t('Header'),
    'navigation' => t('Navigation'),
    'content' => t('Content'),
    'footer' => t('Footer'),
    'footer_bottom' => t('Footer bottom'),
  ),
);

/**
 * Implements minima_layout_LAYOUT_NAME().
 */
function minima_layout_mpc_core_site_default($variables) {
  $layout = array(
    'header_container' => array(
      '#theme_wrappers' => array('minima_grid', 'minima_container'),
      '#container_element' => 'header',
      '#container_attributes' => array(
        'class' => array('Container--header'),
      ),
      'header' => array(
        '#theme_wrappers' => array('minima_grid_cell'),
        '#markup' => $variables['content']['header'],
      ),
    ),
    'navigation_container' => array(
      '#theme_wrappers' => array('minima_container'),
      '#container_attributes' => array(
        'class' => array('Container--navigation', 'u-textCenter'),
      ),
      'navigation' => array(
        '#markup' => $variables['content']['navigation'],
      ),
    ),
    'main' => array(
      '#markup' => $variables['content']['content'],
    ),
    'footer_container' => array(
      '#theme_wrappers' => array('minima_grid', 'minima_container'),
      '#container_element' => 'footer',
      '#container_attributes' => array(
        'class' => array('Container--footer'),
      ),
      '#grid_attributes' => array(
        'class' => array('Grid--space'),
      ),
      'footer' => array(
        '#markup' => $variables['content']['footer'],
      ),
    ),
    'footer_bottom_container' => array(
      '#theme_wrappers' => array('minima_grid', 'minima_container'),
      '#container_attributes' => array(
        'class' => array('Container--footerBottom'),
      ),
      'footer_bottom' => array(
        '#theme_wrappers' => array('minima_grid_cell'),
        '#markup' => $variables['content']['footer_bottom'],
      ),
    ),
  );

  // If current page isn't handled by page manager wrap content in a container.
  $current_page = page_manager_get_current_page();

  if (empty($current_page)) {
    $layout['main'] = array(
      'content_header' => array(
        '#theme' => 'pane_content_header',
      ),
      'content' => array(
        '#markup' => $variables['content']['content'],
        '#theme_wrappers' => array('minima_container'),
        '#container_element' => 'main',
        '#container_attributes' => array(
          'class' => array('Container--main'),
        ),
      ),
    );
  }

  return $layout;
}
