<?php

$plugin = array(
  'title' => 'MPC: Rendered Node/Location Info',
  'category' => 'Metropolitan Pub Company',
  'single' => TRUE,
);

function mpc_core_rendered_node_location_info_content_type_render($subtype, $conf, $args, $context) {
  ctools_plugin_load_function('ctools', 'content_types', 'gk_locations_info', 'render callback');

  if ($node = menu_get_object()) {
    if ($location_info = gk_locations_gk_locations_info_content_type_render($subtype, $conf, $args, $context)) {
      return (object) array(
        'title' => '',
        'content' => array(
          'rendered_node_location_info' => array(
            '#theme_wrappers' => array('minima_grid'),
            '#grid_attributes' => array(
              'class' => array('Grid--space'),
            ),
            'node' => array(
              '#theme_wrappers' => array('minima_grid_cell'),
              '#grid_cell_attributes' => array(
                'class' => array('u-xl-size2of3', 'u-lg-size2of3')
              ),
              'content' => node_view($node),
            ),
            'location_info' => array(
              '#theme_wrappers' => array('container', 'minima_grid_cell'),
              '#attributes' => array(
                'class' => array('Box', 'Box--locationInfo'),
              ),
              '#grid_cell_attributes' => array(
                'class' => array('u-xl-size1of3', 'u-lg-size1of3')
              ),
              'box' => array(
                '#theme_wrappers' => array('container'),
                '#attributes' => array(
                  'class' => array('Box-content'),
                ),
                'content' => $location_info->content,
              ),
            ),
          ),
        ),
      );
    }
  }
}

function mpc_core_rendered_node_location_info_content_type_edit_form($form, &$form_state) {
  form_load_include($form_state, 'inc', 'gk_locations', 'plugins/content_types/gk_locations_info');
  $form += gk_locations_gk_locations_info_content_type_edit_form($form, $form_state);

  return $form;
}

function mpc_core_rendered_node_location_info_content_type_edit_form_submit($form, &$form_state) {
  form_state_values_clean($form_state);
  $form_state['conf'] = $form_state['values'];
  $form_state['conf']['sections'] = array_filter($form_state['conf']['sections']);
}
