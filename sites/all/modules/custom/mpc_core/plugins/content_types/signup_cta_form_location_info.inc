<?php

$plugin = array(
  'title' => 'MPC: Sign-up CTA form/Location Info',
  'category' => 'Metropolitan Pub Company',
  'single' => TRUE,
);

function mpc_core_signup_cta_form_location_info_content_type_render($subtype, $conf, $args, $context) {
  $content = array(
    'signup_cta_form' => drupal_get_form('gk_members_signup_cta_form'),
  );

  ctools_plugin_load_function('ctools', 'content_types', 'gk_locations_info', 'render callback');

  if ($location_info = gk_locations_gk_locations_info_content_type_render($subtype, $conf, $args, $context)) {
    $content['location_info'] = $location_info->content;
  }

  return (object) array(
    'title' => '',
    'content' => $content,
  );
}

function mpc_core_signup_cta_form_location_info_content_type_edit_form($form, &$form_state) {
  form_load_include($form_state, 'inc', 'gk_locations', 'plugins/content_types/gk_locations_info');
  $form += gk_locations_gk_locations_info_content_type_edit_form($form, $form_state);

  return $form;
}

function mpc_core_signup_cta_form_location_info_content_type_edit_form_submit($form, &$form_state) {
  form_state_values_clean($form_state);
  $form_state['conf'] = $form_state['values'];
  $form_state['conf']['sections'] = array_filter($form_state['conf']['sections']);
}
