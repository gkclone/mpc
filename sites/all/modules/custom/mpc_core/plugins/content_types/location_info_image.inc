<?php

$plugin = array(
  'title' => 'MPC: Location Info Image',
  'category' => 'Metropolitan Pub Company',
  'single' => TRUE,
);

function mpc_core_location_info_image_content_type_render($subtype, $conf, $args, $context) {
  ctools_plugin_load_function('ctools', 'content_types', 'gk_locations_info', 'render callback');
  if ($location = gk_locations_get_current_location()) {
    if ($location_info = gk_locations_gk_locations_info_content_type_render($subtype, $conf, $args, $context)) {
      $content = (object) array(
        'title' => '',
        'content' => array(
          'location_node_body_location_info_image' => array(
            '#theme_wrappers' => array('minima_grid'),
            '#grid_attributes' => array(
              'class' => array('Grid--space'),
            ),
            'content' => $location_info->content,
          ),
        ),
      );

      if ($image = file_load($conf['image'])) {
        $content->content['image'] = $image->uri;
      }

      $location_nodes = &$content->content['location_node_body_location_info_image']['content'];
      $cell_count = count($location_nodes);
      $cell_count = ($cell_count > 4) ? 4 : $cell_count;
      $cell_count_md = ($cell_count > 2) ? ceil($cell_count/2) : $cell_count;
      foreach ($location_nodes as &$location_node) {
        $location_node['#theme_wrappers'] = array('minima_grid_cell');
        $location_node['#grid_cell_attributes'] = array(
          'class' => array("u-md-size1of$cell_count_md", "u-lg-size1of$cell_count", "u-xl-size1of$cell_count")
        );
      }

      return $content;
    }
  }
}

function mpc_core_location_info_image_content_type_edit_form($form, &$form_state) {
  // This is required because Drupal won't load this file the next time it needs
  // to build this form (i.e. during AJAX requests invoked by the file field).
  form_load_include($form_state, 'inc', 'mpc_core', 'plugins/content_types/location_info_image');

  form_load_include($form_state, 'inc', 'gk_locations', 'plugins/content_types/gk_locations_info');
  $form += gk_locations_gk_locations_info_content_type_edit_form($form, $form_state);

  $conf = $form_state['conf'];
  $default_value_image = isset($conf['image']) ? $conf['image'] : NULL;

  $form['image'] = array(
    '#title' => t('Image'),
    '#type' => 'managed_file',
    '#upload_location' => 'public://panes/',
    '#default_value' => $default_value_image,
    '#weight' => -10,
  );

  return $form;
}

function mpc_core_location_info_image_content_type_edit_form_submit($form, &$form_state) {
  form_state_values_clean($form_state);
  $form_state['conf'] = $form_state['values'];
  $form_state['conf']['sections'] = array_filter($form_state['conf']['sections']);
}
