<?php

$plugin = array(
  'title' => 'MPC: Rendered Node/Booking Widget',
  'category' => 'Metropolitan Pub Company',
  'single' => TRUE,
);

function mpc_core_rendered_node_booking_widget_content_type_render($subtype, $conf, $args, $context) {
  if ($node = menu_get_object()) {
    ctools_plugin_load_function('ctools', 'content_types', 'gk_table_booking_widget', 'render callback');

    if ($booking_widget = gk_table_booking_gk_table_booking_widget_content_type_render($subtype, $conf, $args, $context)) {
      return (object) array(
        'title' => '',
        'content' => array(
          'rendered_node_location_info' => array(
            '#theme_wrappers' => array('minima_grid'),
            '#grid_attributes' => array(
              'class' => array('Grid--space'),
            ),
            'node' => array(
              '#theme_wrappers' => array('minima_grid_cell'),
              '#grid_cell_attributes' => array(
                'class' => array('u-xl-size2of3', 'u-lg-size2of3')
              ),
              'content' => node_view($node),
            ),
            'booking_widget' => array(
              '#theme_wrappers' => array('container', 'minima_grid_cell'),
              '#attributes' => array(
                'class' => array('Box', 'Box--bookingWidget'),
              ),
              '#grid_cell_attributes' => array(
                'class' => array('u-xl-size1of3', 'u-lg-size1of3')
              ),
              'box' => array(
                '#theme_wrappers' => array('container'),
                '#attributes' => array(
                  'class' => array('Box-content'),
                ),
                'content' => $booking_widget->content,
              ),
            ),
          ),
        ),
      );
    }
  }
}
