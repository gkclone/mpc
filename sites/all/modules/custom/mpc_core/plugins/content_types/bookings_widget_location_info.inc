<?php

$plugin = array(
  'title' => 'MPC: Bookings Widget/Location Info',
  'category' => 'Metropolitan Pub Company',
  'single' => TRUE,
);

function mpc_core_bookings_widget_location_info_content_type_render($subtype, $conf, $args, $context) {
  $content = array(
    'grid' => array(
      '#theme_wrappers' => array('minima_grid'),
      'grid_cell' => array(
        '#theme_wrappers' => array('minima_grid_cell'),
        'box' => array(
          '#theme_wrappers' => array('container'),
          '#attributes' => array(
            'class' => array('Box', 'Box--bookingWidgetLocationInfo'),
          ),
          'title' => array(
            '#theme' => 'html_tag',
            '#tag' => 'h2',
            '#value' => t('Bookings'),
            '#attributes' => array(
              'class' => array(
                'Box-title',
              ),
            ),
            '#weight' => -10,
          ),
          'grid' => array(
            '#theme_wrappers' => array('minima_grid'),
          ),
        ),
      ),
    ),
  );

  if ($image = file_load($conf['image'])) {
    $content['grid']['background_image'] = $image->uri;
  }

  // Location info.
  ctools_plugin_load_function('ctools', 'content_types', 'gk_locations_info', 'render callback');

  if ($location_info = gk_locations_gk_locations_info_content_type_render($subtype, $conf, $args, $context)) {
    $content['grid']['grid_cell']['box']['grid']['location_info'] = array(
      '#theme_wrappers' => array('minima_grid_cell'),
      'content' => $location_info->content,
    );
  }

  // Booking widget.
  ctools_plugin_load_function('ctools', 'content_types', 'gk_table_booking_widget', 'render callback');

  if ($booking_widget = gk_table_booking_gk_table_booking_widget_content_type_render($subtype, $conf, $args, $context)) {
    $content['grid']['grid_cell']['box']['grid']['booking_widget'] = array(
      '#theme_wrappers' => array('minima_grid_cell'),
      'content' => $booking_widget->content,
    );
  }

  return (object) array(
    'content' => $content,
  );
}

function mpc_core_bookings_widget_location_info_content_type_edit_form($form, &$form_state) {
  // This is required because Drupal won't load this file the next time it needs
  // to build this form (i.e. during AJAX requests invoked by the file field).
  form_load_include($form_state, 'inc', 'mpc_core', 'plugins/content_types/bookings_widget_location_info');

  form_load_include($form_state, 'inc', 'gk_locations', 'plugins/content_types/gk_locations_info');
  $form += gk_locations_gk_locations_info_content_type_edit_form($form, $form_state);

  $conf = $form_state['conf'];
  $default_value_image = isset($conf['image']) ? $conf['image'] : NULL;

  $form['image'] = array(
    '#title' => t('Image'),
    '#type' => 'managed_file',
    '#upload_location' => 'public://panes/',
    '#default_value' => $default_value_image,
    '#weight' => -10,
  );

  $form['booking_widget'] = array(
    '#title' => t('Display booking widget?'),
    '#type' => 'checkbox',
    '#default_value' => $conf['booking_widget'] ? $conf['booking_widget'] : 0,
  );

  return $form;
}

function mpc_core_bookings_widget_location_info_content_type_edit_form_submit($form, &$form_state) {
  form_state_values_clean($form_state);
  $form_state['conf'] = $form_state['values'];
  $form_state['conf']['sections'] = array_filter($form_state['conf']['sections']);

  // Make the uploaded file permanent.
  if ($file = file_load($form_state['values']['image'])) {
    $file->status = FILE_STATUS_PERMANENT;
    file_save($file);
    file_usage_add($file, 'mpc_core', 'pane', $file->fid);
  }
}
