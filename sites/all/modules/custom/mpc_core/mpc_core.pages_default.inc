<?php
/**
 * @file
 * mpc_core.pages_default.inc
 */

/**
 * Implements hook_default_page_manager_handlers().
 */
function mpc_core_default_page_manager_handlers() {
  $export = array();

  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'site_template_panel_context';
  $handler->task = 'site_template';
  $handler->subtask = '';
  $handler->handler = 'panel_context';
  $handler->weight = -30;
  $handler->conf = array(
    'title' => 'MPC: Default',
    'no_blocks' => 0,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
  );
  $display = new panels_display();
  $display->layout = 'mpc_core_site_default';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'header' => NULL,
      'navigation' => NULL,
      'content' => NULL,
      'footer' => NULL,
      'footer_bottom' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '';
  $display->uuid = 'bab78473-9c0a-4ced-83c6-9fc7c19e7e12';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-fe94ea69-1bdb-43da-bcfd-44c236502a6d';
    $pane->panel = 'content';
    $pane->type = 'page_content';
    $pane->subtype = 'page_content';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'context' => 'argument_page_content_1',
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'fe94ea69-1bdb-43da-bcfd-44c236502a6d';
    $display->content['new-fe94ea69-1bdb-43da-bcfd-44c236502a6d'] = $pane;
    $display->panels['content'][0] = 'new-fe94ea69-1bdb-43da-bcfd-44c236502a6d';
    $pane = new stdClass();
    $pane->pid = 'new-6d91d007-1de6-4ba3-baac-1f9d016fb2a0';
    $pane->panel = 'footer';
    $pane->type = 'block';
    $pane->subtype = 'domain_menu_block-footer_menu';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '6d91d007-1de6-4ba3-baac-1f9d016fb2a0';
    $display->content['new-6d91d007-1de6-4ba3-baac-1f9d016fb2a0'] = $pane;
    $display->panels['footer'][0] = 'new-6d91d007-1de6-4ba3-baac-1f9d016fb2a0';
    $pane = new stdClass();
    $pane->pid = 'new-12c1fd46-7cd2-4d4d-bfdc-3565e40cad8d';
    $pane->panel = 'footer';
    $pane->type = 'gk_locations_info';
    $pane->subtype = 'gk_locations_info';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => '',
      'intro' => array(
        'value' => '',
        'format' => 'full_html',
      ),
      'sections' => array(
        'contact_details' => array(
          'display' => 1,
          'override_title' => TRUE,
          'override_title_text' => '',
          'weight' => '0',
        ),
        'address' => array(
          'display' => 1,
          'override_title' => TRUE,
          'override_title_text' => '',
          'weight' => '1',
          'view_map_link' => 0,
        ),
        'opening_hours' => array(
          'display' => 0,
          'override_title' => 0,
          'override_title_text' => '',
          'weight' => '0',
        ),
        'food_hours' => array(
          'display' => 0,
          'override_title' => 0,
          'override_title_text' => '',
          'weight' => '0',
        ),
        'facilities' => array(
          'display' => 0,
          'override_title' => 0,
          'override_title_text' => '',
          'weight' => '0',
        ),
        'social' => array(
          'display' => 0,
          'override_title' => 0,
          'override_title_text' => '',
          'weight' => '0',
        ),
      ),
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = '12c1fd46-7cd2-4d4d-bfdc-3565e40cad8d';
    $display->content['new-12c1fd46-7cd2-4d4d-bfdc-3565e40cad8d'] = $pane;
    $display->panels['footer'][1] = 'new-12c1fd46-7cd2-4d4d-bfdc-3565e40cad8d';
    $pane = new stdClass();
    $pane->pid = 'new-a2ce5c69-9883-44b2-a4f1-3629e5073726';
    $pane->panel = 'footer';
    $pane->type = 'signup_cta_form_location_info';
    $pane->subtype = 'signup_cta_form_location_info';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 0,
      'override_title_text' => '',
      'intro' => array(
        'value' => '',
        'format' => 'full_html',
      ),
      'sections' => array(
        'contact_details' => array(
          'display' => 0,
          'override_title' => 0,
          'override_title_text' => '',
          'weight' => '0',
        ),
        'address' => array(
          'display' => 0,
          'override_title' => 0,
          'override_title_text' => '',
          'weight' => '0',
          'view_map_link' => 0,
        ),
        'opening_hours' => array(
          'display' => 0,
          'override_title' => 0,
          'override_title_text' => '',
          'weight' => '0',
        ),
        'food_hours' => array(
          'display' => 0,
          'override_title' => 0,
          'override_title_text' => '',
          'weight' => '0',
        ),
        'facilities' => array(
          'display' => 0,
          'override_title' => 0,
          'override_title_text' => '',
          'weight' => '0',
        ),
        'social' => array(
          'display' => 1,
          'override_title' => 0,
          'override_title_text' => '',
          'weight' => '0',
        ),
      ),
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 2;
    $pane->locks = array();
    $pane->uuid = 'a2ce5c69-9883-44b2-a4f1-3629e5073726';
    $display->content['new-a2ce5c69-9883-44b2-a4f1-3629e5073726'] = $pane;
    $display->panels['footer'][2] = 'new-a2ce5c69-9883-44b2-a4f1-3629e5073726';
    $pane = new stdClass();
    $pane->pid = 'new-320ab76c-2353-4611-b762-8f69430ea80b';
    $pane->panel = 'footer_bottom';
    $pane->type = 'block';
    $pane->subtype = 'gk_core-copyright';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '320ab76c-2353-4611-b762-8f69430ea80b';
    $display->content['new-320ab76c-2353-4611-b762-8f69430ea80b'] = $pane;
    $display->panels['footer_bottom'][0] = 'new-320ab76c-2353-4611-b762-8f69430ea80b';
    $pane = new stdClass();
    $pane->pid = 'new-4442897e-1efc-4ba7-bac0-5692e303632e';
    $pane->panel = 'header';
    $pane->type = 'pane_header';
    $pane->subtype = 'pane_header';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array();
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '4442897e-1efc-4ba7-bac0-5692e303632e';
    $display->content['new-4442897e-1efc-4ba7-bac0-5692e303632e'] = $pane;
    $display->panels['header'][0] = 'new-4442897e-1efc-4ba7-bac0-5692e303632e';
    $pane = new stdClass();
    $pane->pid = 'new-9b3ba725-aef2-45bd-ac72-356702dec0e2';
    $pane->panel = 'navigation';
    $pane->type = 'block';
    $pane->subtype = 'domain_menu_block-main_menu';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '9b3ba725-aef2-45bd-ac72-356702dec0e2';
    $display->content['new-9b3ba725-aef2-45bd-ac72-356702dec0e2'] = $pane;
    $display->panels['navigation'][0] = 'new-9b3ba725-aef2-45bd-ac72-356702dec0e2';
  $display->hide_title = PANELS_TITLE_NONE;
  $display->title_pane = '0';
  $handler->conf['display'] = $display;
  $export['site_template_panel_context'] = $handler;

  return $export;
}
