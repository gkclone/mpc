<?php
/**
 * @file
 * mpc_pages.panelizer.inc
 */

/**
 * Implements hook_panelizer_defaults().
 */
function mpc_pages_panelizer_defaults() {
  $export = array();

  $panelizer = new stdClass();
  $panelizer->disabled = FALSE; /* Edit this to true to make a default panelizer disabled initially */
  $panelizer->api_version = 1;
  $panelizer->name = 'node:page:mpc_pages_home';
  $panelizer->title = 'MPC: Home';
  $panelizer->panelizer_type = 'node';
  $panelizer->panelizer_key = 'page';
  $panelizer->no_blocks = FALSE;
  $panelizer->css_id = '';
  $panelizer->css = '';
  $panelizer->pipeline = 'standard';
  $panelizer->contexts = array();
  $panelizer->relationships = array();
  $panelizer->access = array();
  $panelizer->view_mode = 'page_manager';
  $panelizer->css_class = '';
  $panelizer->title_element = 'H2';
  $panelizer->link_to_entity = TRUE;
  $panelizer->extra = array();
  $display = new panels_display();
  $display->layout = '1_column';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'primary' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '%node:title';
  $display->uuid = '3713755a-9db4-43d5-a7ac-238bacc507e5';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-e7a5eac2-fe9a-4a0e-bd80-8cc5f2101976';
    $pane->panel = 'primary';
    $pane->type = 'pane_content_header';
    $pane->subtype = 'pane_content_header';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array();
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'e7a5eac2-fe9a-4a0e-bd80-8cc5f2101976';
    $display->content['new-e7a5eac2-fe9a-4a0e-bd80-8cc5f2101976'] = $pane;
    $display->panels['primary'][0] = 'new-e7a5eac2-fe9a-4a0e-bd80-8cc5f2101976';
    $pane = new stdClass();
    $pane->pid = 'new-da51bdc7-b1e0-49c5-b6a5-40f44519a906';
    $pane->panel = 'primary';
    $pane->type = 'location_node_body_location_info';
    $pane->subtype = 'location_node_body_location_info';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 0,
      'override_title_text' => '',
      'intro' => array(
        'value' => '',
        'format' => 'full_html',
      ),
      'sections' => array(
        'contact_details' => array(
          'display' => 0,
          'override_title' => 0,
          'override_title_text' => '',
          'weight' => '0',
        ),
        'address' => array(
          'display' => 1,
          'override_title' => TRUE,
          'override_title_text' => 'Where to find us',
          'weight' => '1',
          'view_map_link' => 1,
        ),
        'opening_hours' => array(
          'display' => 1,
          'override_title' => 0,
          'override_title_text' => '',
          'weight' => '0',
        ),
        'food_hours' => array(
          'display' => 0,
          'override_title' => 0,
          'override_title_text' => '',
          'weight' => '0',
        ),
        'facilities' => array(
          'display' => 0,
          'override_title' => 0,
          'override_title_text' => '',
          'weight' => '0',
        ),
        'social' => array(
          'display' => 0,
          'override_title' => 0,
          'override_title_text' => '',
          'weight' => '0',
        ),
      ),
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = 'da51bdc7-b1e0-49c5-b6a5-40f44519a906';
    $display->content['new-da51bdc7-b1e0-49c5-b6a5-40f44519a906'] = $pane;
    $display->panels['primary'][1] = 'new-da51bdc7-b1e0-49c5-b6a5-40f44519a906';
    $pane = new stdClass();
    $pane->pid = 'new-f1c512fd-1223-448d-b7dd-41991862a5e9';
    $pane->panel = 'primary';
    $pane->type = 'gk_events_list';
    $pane->subtype = 'gk_events_list';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => 'Upcoming Events',
      'view_mode' => 'promote',
      'type' => 'upcoming',
      'promote' => '1',
      'sticky' => NULL,
      'paged' => 0,
      'per_page' => '5',
      'limit' => '3',
      'exclude_current_node' => 0,
      'hide_dates' => 0,
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 2;
    $pane->locks = array();
    $pane->uuid = 'f1c512fd-1223-448d-b7dd-41991862a5e9';
    $display->content['new-f1c512fd-1223-448d-b7dd-41991862a5e9'] = $pane;
    $display->panels['primary'][2] = 'new-f1c512fd-1223-448d-b7dd-41991862a5e9';
    $pane = new stdClass();
    $pane->pid = 'new-02c7fc8c-d262-46f3-ac8e-1d7e80f50cd0';
    $pane->panel = 'primary';
    $pane->type = 'gk_members_signup_cta_form';
    $pane->subtype = 'gk_members_signup_cta_form';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 3;
    $pane->locks = array();
    $pane->uuid = '02c7fc8c-d262-46f3-ac8e-1d7e80f50cd0';
    $display->content['new-02c7fc8c-d262-46f3-ac8e-1d7e80f50cd0'] = $pane;
    $display->panels['primary'][3] = 'new-02c7fc8c-d262-46f3-ac8e-1d7e80f50cd0';
    $pane = new stdClass();
    $pane->pid = 'new-dcbda442-48b0-410a-ad22-d9d603622ef0';
    $pane->panel = 'primary';
    $pane->type = 'bookings_widget_location_info';
    $pane->subtype = 'bookings_widget_location_info';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 0,
      'override_title_text' => '',
      'intro' => array(
        'value' => '<p>Book a table in our stunning dining room to make sure that you don&rsquo;t miss out.</p>
',
        'format' => 'full_html',
      ),
      'sections' => array(
        'contact_details' => array(
          'display' => 0,
          'override_title' => 0,
          'override_title_text' => '',
          'weight' => '0',
        ),
        'address' => array(
          'display' => 0,
          'override_title' => 0,
          'override_title_text' => '',
          'weight' => '0',
          'view_map_link' => 0,
        ),
        'opening_hours' => array(
          'display' => 0,
          'override_title' => 0,
          'override_title_text' => '',
          'weight' => '0',
        ),
        'food_hours' => array(
          'display' => 1,
          'override_title' => TRUE,
          'override_title_text' => 'Food hours',
          'weight' => '1',
        ),
        'facilities' => array(
          'display' => 0,
          'override_title' => 0,
          'override_title_text' => '',
          'weight' => '0',
        ),
        'social' => array(
          'display' => 0,
          'override_title' => 0,
          'override_title_text' => '',
          'weight' => '0',
        ),
      ),
      'image' => 0,
      'booking_widget' => 1,
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 4;
    $pane->locks = array();
    $pane->uuid = 'dcbda442-48b0-410a-ad22-d9d603622ef0';
    $display->content['new-dcbda442-48b0-410a-ad22-d9d603622ef0'] = $pane;
    $display->panels['primary'][4] = 'new-dcbda442-48b0-410a-ad22-d9d603622ef0';
    $pane = new stdClass();
    $pane->pid = 'new-c213658e-40bc-40ee-a842-f74244d32818';
    $pane->panel = 'primary';
    $pane->type = 'gk_secret_dj_now_playing';
    $pane->subtype = 'gk_secret_dj_now_playing';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => 'What\'s playing?',
      'limit' => '1',
      'content' => array(
        'value' => 'Using the Secret DJ app you can add your favourite tracks to our digital jukebox live, in the pub. Come on down and get involved!',
        'format' => 'full_html',
      ),
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 5;
    $pane->locks = array();
    $pane->uuid = 'c213658e-40bc-40ee-a842-f74244d32818';
    $display->content['new-c213658e-40bc-40ee-a842-f74244d32818'] = $pane;
    $display->panels['primary'][5] = 'new-c213658e-40bc-40ee-a842-f74244d32818';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = '0';
  $panelizer->display = $display;
  $export['node:page:mpc_pages_home'] = $panelizer;

  return $export;
}
