;(function($) {
  Drupal.behaviors.mpc_color_picker = {
    attach: function(context, settings) {
      var $form = $('#system-theme-settings', context);

      for (var i = 0; i < settings.mpc_color_picker.fieldIDs.length; i++) {
        var fieldID = settings.mpc_color_picker.fieldIDs[i],
            colorPicker = $.farbtastic($('.ColorPicker.ColorPicker--' + fieldID, $form));

        colorPicker.linkTo($('input[name="minima_config[settings][' + fieldID + ']"]'));
      };
    }
  };
})(jQuery);
