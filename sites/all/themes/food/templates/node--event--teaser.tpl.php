<?php if ($content): ?>
  <div<?php print $attributes; ?>>
    <div class="Grid Grid--space">
      <div class="Grid-cell u-xl-size1of6">
        <div class="Node-date">
          <?php print render($field_event_date); ?>
        </div>
      </div>
      <div class="Grid-cell u-xl-size3of6 u-lg-size4of6">
        <?php if ($display_title): ?>
          <h2 class="Node-title">
            <?php print $title; ?>
          </h2>
        <?php endif; ?>
        <div<?php print $content_attributes; ?>>
          <?php print $body; ?>
        </div>
      </div>
      <div class="Grid-cell u-xl-size2of6 u-lg-size2of6">
        <?php if (!empty($links)): ?>
          <div class="u-textRight">
            <?php print $links; ?>
          </div>
        <?php endif; ?>
      </div>
    </div>
  </div>
<?php endif; ?>
