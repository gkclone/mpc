<div<?php print $attributes; ?>>
  <h2<?php print $title_attributes; ?>><?php print $title; ?></h2>
  <div class="Grid Grid--spaceHorizontal">
    <div class="Grid-cell u-xl-size3of5 u-lg-size3of5 u-ie-size3of5">
      <div class="Promotion-content">
        <?php print $body; ?>
      </div>
    </div>
    <div class="Grid-cell u-xl-size2of5 u-lg-size2of5 u-ie-size2of5">
      <?php if ($cta && $is_link): ?>
        <div class="Promotion-cta">
          <a<?php print $link_attributes; ?>>
           <?php print $cta; ?>
          </a>
        </div>
      <?php endif; ?>
    </div>
  </div>
</div>