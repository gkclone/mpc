<?php if ($content): ?>
  <div<?php print $attributes; ?>>
    <div class="Node-date">
      <?php print render($field_event_date); ?>
    </div>
    <?php if ($display_title): ?>
      <h2 class="Node-title">
        <?php print $title; ?>
      </h2>
    <?php endif; ?>
    <div<?php print $content_attributes; ?>>
      <?php print $body; ?>
      <?php if (!empty($links)): ?>
        <?php print $links; ?>
      <?php endif; ?>
    </div>
  </div>
<?php endif; ?>
