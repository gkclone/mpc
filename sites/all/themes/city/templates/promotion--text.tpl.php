<div<?php print $attributes; ?>>
  <div class="Promotion-content">
    <h2<?php print $title_attributes; ?>><?php print $title; ?></h2>
    <?php print $body; ?>
    <?php if ($cta && $is_link): ?>
      <div class="Promotion-cta">
        <a<?php print $link_attributes; ?>>
         <?php print $cta; ?>
        </a>
      </div>
    <?php endif; ?>
  </div>
</div>
