<div class="MatchPintItem">
  <div class="Grid Grid--spaceHorizontal Grid--alignMiddle">
    <div class="Grid-cell u-xl-size3of12">
      <div class="MatchPintItem-sport">
        <?php print($sport); ?>
      </div>
    </div>
    <div class="Grid-cell u-size1of6 u-lg-size2of12 u-xl-size2of12">
      <div class="MatchPintItem-image MatchPintItem-image--teamOne">
        <?php print($team_image_1); ?>
      </div>
    </div>
    <div class="Grid-cell u-lg-size4of6 u-xl-size3of12">
      <div class="MatchPintItem-title">
        <?php print($title) ?>
      </div>
    </div>
    <div class="Grid-cell u-size1of6 u-lg-size2of12 u-xl-size2of12">
      <div class="MatchPintItem-image MatchPintItem-image--teamTwo">
        <?php print($team_image_2); ?>
      </div>
    </div>
    <div class="Grid-cell u-xl-size2of12">
      <div class="MatchPintItem-time">
        <?php print(format_date(strtotime($date), 'custom', 'H:i')) ?>
      </div>
    </div>
  </div>
</div>
