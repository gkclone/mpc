<?php

/**
 * Implements hook_minima_config_settings().
 */
function city_minima_config_settings() {
  $color_scheme_options = array(
    'header_background' => array(
      'title' => 'Header Background',
      'variable' => 'header-background',
    ),
    'body_background' => array(
      'title' => 'Body Background',
      'variable' => 'body-background',
    ),
    'offcanvas_handle_color' => array(
      'title' => 'Mobile Menu Icon Color',
      'variable' => 'offcanvas-handle-color',
    ),
    'color_one_style_one' => array(
      'title' => 'Background Color, Style One',
      'variable' => 'config--color-one--style-one',
    ),
    'color_two_style_one' => array(
      'title' => 'Text Color, Style One',
      'variable' => 'config--color-two--style-one',
    ),
    'color_three_style_one' => array(
      'title' => 'Hover Color/Headings Color, Style One',
      'variable' => 'config--color-three--style-one',
    ),
    'color_four_style_one' => array(
      'title' => 'Accent Color, Style One',
      'variable' => 'config--color-four--style-one',
    ),
    'color_one_style_two' => array(
      'title' => 'Background Color, Style Two',
      'variable' => 'config--color-one--style-two',
    ),
    'color_two_style_two' => array(
      'title' => 'Text Color 1, Style Two',
      'variable' => 'config--color-two--style-two',
    ),
    'color_three_style_two' => array(
      'title' => 'Text Color 2, Style Two',
      'variable' => 'config--color-three--style-two',
    ),
    'color_four_style_two' => array(
      'title' => 'Headings Color, Style Two',
      'variable' => 'config--color-four--style-two',
    ),
    'color_five_style_two' => array(
      'title' => 'Accent Color, Style Two',
      'variable' => 'config--color-five--style-two',
    ),
    'color_one_style_three' => array(
      'title' => 'Background Color 1, Style Three',
      'variable' => 'config--color-one--style-three',
    ),
    'color_two_style_three' => array(
      'title' => 'Background Color 2, Style Three',
      'variable' => 'config--color-two--style-three',
    ),
    'color_three_style_three' => array(
      'title' => 'Text Color, Style Three',
      'variable' => 'config--color-three--style-three',
    ),
    'color_four_style_three' => array(
      'title' => 'Hover Color/Headings Color, Style Three',
      'variable' => 'config--color-four--style-three',
    ),
    'color_one_style_four' => array(
      'title' => 'Background Color, Style Four',
      'variable' => 'config--color-one--style-four',
    ),
    'color_two_style_four' => array(
      'title' => 'Text Color 1, Style Four',
      'variable' => 'config--color-two--style-four',
    ),
    'color_three_style_four' => array(
      'title' => 'Text Color 2, Style Four',
      'variable' => 'config--color-three--style-four',
    ),
    'color_four_style_four' => array(
      'title' => 'Headings Color, Style Four',
      'variable' => 'config--color-four--style-four',
    ),
    'color_five_style_four' => array(
      'title' => 'Accent Color, Style Four',
      'variable' => 'config--color-five--style-four',
    ),
    'color_one_style_five' => array(
      'title' => 'Background Color, Style Five',
      'variable' => 'config--color-one--style-five',
    ),
    'color_two_style_five' => array(
      'title' => 'Text Color, Style Five',
      'variable' => 'config--color-two--style-five',
    ),
    'color_three_style_five' => array(
      'title' => 'Hover Color/Headings Color, Style Five',
      'variable' => 'config--color-three--style-five',
    ),
    'color_four_style_five' => array(
      'title' => 'Accent Color, Style Five',
      'variable' => 'config--color-four--style-five',
    ),
    'color_one_style_six' => array(
      'title' => 'Background Color, Style Six',
      'variable' => 'config--color-one--style-six',
    ),
    'color_two_style_six' => array(
      'title' => 'Text Color 1, Style Six',
      'variable' => 'config--color-two--style-six',
    ),
    'color_three_style_six' => array(
      'title' => 'Text Color 2, Style Six',
      'variable' => 'config--color-three--style-six',
    ),
    'color_four_style_six' => array(
      'title' => 'Headings Color, Style Six',
      'variable' => 'config--color-four--style-six',
    ),
    'color_five_style_six' => array(
      'title' => 'Accent Color, Style Six',
      'variable' => 'config--color-five--style-six',
    ),
    'color_one_style_seven' => array(
      'title' => 'Background Color, Style Seven',
      'variable' => 'config--color-one--style-seven',
    ),
    'color_two_style_seven' => array(
      'title' => 'Text Color, Style Seven',
      'variable' => 'config--color-two--style-seven',
    ),
    'color_three_style_seven' => array(
      'title' => 'Headings Color, Style Seven',
      'variable' => 'config--color-three--style-seven',
    ),
    'color_four_style_seven' => array(
      'title' => 'Accent Color, Style Seven',
      'variable' => 'config--color-four--style-seven',
    ),
    'color_one_style_eight' => array(
      'title' => 'Background Color, Style Eight',
      'variable' => 'config--color-one--style-eight',
    ),
    'color_two_style_eight' => array(
      'title' => 'Text Color 1, Style Eight',
      'variable' => 'config--color-two--style-eight',
    ),
    'color_three_style_eight' => array(
      'title' => 'Text Color 2, Style Eight',
      'variable' => 'config--color-three--style-eight',
    ),
    'color_four_style_eight' => array(
      'title' => 'Headings Color, Style Eight',
      'variable' => 'config--color-four--style-eight',
    ),
    'color_five_style_eight' => array(
      'title' => 'Accent Color, Style Eight',
      'variable' => 'config--color-five--style-eight',
    ),
    'color_one_style_nine' => array(
      'title' => 'Background Color, Style Nine',
      'variable' => 'config--color-one--style-nine',
    ),
    'color_two_style_nine' => array(
      'title' => 'Text Color, Style Nine',
      'variable' => 'config--color-two--style-nine',
    ),
    'color_three_style_nine' => array(
      'title' => 'Accent Color, Style Nine',
      'variable' => 'config--color-three--style-nine',
    ),
  );

  $available_colors = array(
    'color_primary_lighter' => array(
      'title' => 'Color Primary Lighter',
      'variable' => '@color-primary-lighter',
    ),
    'color_primary_light' => array(
      'title' => 'Color Primary Light',
      'variable' => '@color-primary-light',
    ),
    'color_primary' => array(
      'title' => 'Color Primary',
      'variable' => '@color-primary',
    ),
    'color_primary_dark' => array(
      'title' => 'Color Primary Dark',
      'variable' => '@color-primary-dark',
    ),
    'color_primary_darker' => array(
      'title' => 'Color Primary Darker',
      'variable' => '@color-primary-darker',
    ),
    'color_secondary_lighter' => array(
      'title' => 'Color Secondary Lighter',
      'variable' => '@color-secondary-lighter',
    ),
    'color_secondary_light' => array(
      'title' => 'Color Secondary Light',
      'variable' => '@color-secondary-light',
    ),
    'color_secondary' => array(
      'title' => 'Color Secondary',
      'variable' => '@color-secondary',
    ),
    'color_secondary_dark' => array(
      'title' => 'Color Secondary Dark',
      'variable' => '@color-secondary-dark',
    ),
    'color_secondary_darker' => array(
      'title' => 'Color Secondary Darker',
      'variable' => '@color-secondary-darker',
    ),
    'color_neutral_lightest' => array(
      'title' => 'Color Neutral Lightest',
      'variable' => '@color-neutral-lightest',
    ),
    'color_neutral_lighter' => array(
      'title' => 'Color Neutral Lighter',
      'variable' => '@color-neutral-lighter',
    ),
    'color_neutral_light' => array(
      'title' => 'Color Neutral Light',
      'variable' => '@color-neutral-light',
    ),
    'color_neutral' => array(
      'title' => 'Color Neutral',
      'variable' => '@color-neutral',
    ),
    'color_neutral_dark' => array(
      'title' => 'Color Neutral Dark',
      'variable' => '@color-neutral-dark',
    ),
    'color_neutral_darker' => array(
      'title' => 'Color Neutral Darker',
      'variable' => '@color-neutral-darker',
    ),
    'color_neutral_darkest' => array(
      'title' => 'Color Neutral Darkest',
      'variable' => '@color-neutral-darkest',
    ),
  );

  foreach ($color_scheme_options as $color_scheme_key => $color_scheme_option) {
    $minima_settings[$color_scheme_key] = array(
      'title' => $color_scheme_option['title'],
    );

    $minima_settings[$color_scheme_key]['options'] = array();

    foreach ($available_colors as $available_color_key => $available_color) {
      $minima_settings[$color_scheme_key]['options'][$available_color_key] = array();
      $minima_settings[$color_scheme_key]['options'][$available_color_key]['title'] = $available_color['title'];
      $minima_settings[$color_scheme_key]['options'][$available_color_key]['variables'][$color_scheme_option['variable']] = $available_color['variable'];
    }
  }

  return $minima_settings;
}

/**
 * Preprocess variables for theme panels_pane.
 */
function city_preprocess_panels_pane(&$variables) {
  switch ($variables['pane']->type) {
    // Add grid cell wrappers for the secret dj feed
    case 'gk_secret_dj_now_playing':
      $secret_dj = &$variables['content']['box_inner']['secret_dj'];

      array_unshift($secret_dj['#theme_wrappers'], 'minima_grid');
      $secret_dj['#grid_attributes'] = array('class' => array('Grid--spaceHorizontal'));

      $secret_dj['song_info_wrapper'] = array(
        'song_info' => $secret_dj['song_info']
      );
      unset($secret_dj['song_info']);

      $secret_dj['song_info_wrapper']['#weight'] = -10;
      $secret_dj['song_info_wrapper']['#theme_wrappers'] = array('container', 'minima_grid_cell');
      $secret_dj['song_info_wrapper']['#attributes'] = array(
        'class' => array('SecretDj-songWrapper'),
      );
      $secret_dj['song_info_wrapper']['#grid_cell_attributes'] = array(
        'class' => array('u-xl-size1of2', 'u-lg-size1of2'),
      );

      $secret_dj['content']['#theme_wrappers'][] = 'minima_grid_cell';
      $secret_dj['content']['#grid_cell_attributes'] = array(
        'class' => array('u-xl-size1of2', 'u-lg-size1of2'),
      );

      if ($variables['title']) {
        $secret_dj['song_info_wrapper']['title'] = array(
          '#theme' => 'html_tag',
          '#tag' => 'h3',
          '#attributes' => array(
            'class' => array('Box-title'),
          ),
          '#value' => $variables['title'],
          '#weight' => -10,
        );
        $variables['title'] = '';
      }
    break;

    // Wrapping events teaser list in grid cells
    case 'gk_events_list':
      $wrapped = FALSE;

      foreach ($variables['content'] as $key => &$item) {
        if (isset($item['#entity_type']) && $item['#entity_type'] == 'node' && ($item['#view_mode'] == 'teaser' OR $item['#view_mode'] == 'promote')) {
          $item['#theme_wrappers'][] = 'minima_grid_cell';
          $item['#grid_cell_attributes']['class'][] = 'u-md-size1of2 u-lg-size1of2 u-xl-size1of4';
          $wrapped = TRUE;
        }
      }

      if ($wrapped) {
        // Move pager into $pager variable
        $pager = $variables['content']['pager'];
        unset($variables['content']['pager']);

        // Wrap events in minima_grid
        $grid = $variables['content'];
        $grid['#theme_wrappers'][] = 'minima_grid';
        $grid['#grid_attributes']['class'] = 'Grid--space';

        // Restructure content
        $variables['content'] = array(
          'grid' => $grid,
          'pager' => $pager,
        );
      }
    break;

    case 'rendered_node_booking_widget':
      $variables['container_attributes_array']['class'][] = 'Container--renderedNodeBookingWidget';
    break;

    // Set booking widget and location info into three grid cells
    case 'bookings_widget_location_info':
      $variables['container_attributes_array']['class'][] = 'Container--banner';

      $grid = &$variables['content']['grid']['grid_cell']['box']['grid'];
      $grid['#grid_attributes'] = array('class' => 'Grid--space');

      $grid['location_info_intro'] = array(
        '#weight' => 0,
        '#theme_wrappers' => array('minima_grid_cell'),
        '#grid_cell_attributes' => array('class' => 'u-lg-size1of3 u-xl-size1of3'),
        'content' => array(
          '#theme_wrappers' => array('container'),
          '#attributes' => array('class' => 'Box'),
          'content' => $grid['location_info']['content']['intro'],
        ),
      );
      unset($grid['location_info']['content']['intro']);

      $grid['location_info_other'] = array(
        '#weight' => 10,
        '#theme_wrappers' => array('minima_grid_cell'),
        '#grid_cell_attributes' => array('class' => 'u-lg-size1of3 u-xl-size1of3'),
        'content' => array(
          '#theme_wrappers' => array('container'),
          '#attributes' => array('class' => 'Box'),
          'content' => $grid['location_info']['content'],
        ),
      );
      unset($grid['location_info']);

      $grid['booking_widget'] = array(
        '#weight' => 20,
        '#theme_wrappers' => array('minima_grid_cell'),
        '#grid_cell_attributes' => array('class' => 'u-lg-size1of3 u-xl-size1of3'),
        'content' => array(
          '#theme_wrappers' => array('container'),
          '#attributes' => array('class' => 'Box'),
          'content' => $grid['booking_widget']['content'],
        ),
      );

      if (!empty($variables['content']['grid']['background_image'])) {
        $url = file_create_url($variables['content']['grid']['background_image']);
        $variables['container_attributes_array']['style'][] = 'background-image: url(' . $url . ');';
      }
    break;

    // Move pane title above location info
    case 'location_node_img_location_info':
      $variables['container_attributes_array']['class'][] = 'Container--locationNodeImageLocationInfo';
      $variables['content']['location_node_body_location_info']['location_info']['title'] = array(
        '#theme' => 'html_tag',
        '#tag' => 'h2',
        '#weight' => -1,
        '#value' => $variables['pane']->configuration['override_title_text'],
        '#attributes' => array('class' => 'Box-title Icon Icon--location'),
      );
      $variables['title'] = false;
    break;

    // Wrapping set packs in grid cells
    case 'set_packs_list':
      if (is_array($variables['content'])) {
        $variables['container_attributes_array']['class'][] = 'Container--primary';
        $nodes = &$variables['content']['set_packs_list']['nodes']['content'];

        // Center grid cells
        $variables['content']['set_packs_list']['#grid_attributes']['class'] = 'Grid--alignCenter';
        $variables['content']['set_packs_list']['nodes']['#theme_wrappers'] = array('minima_grid_cell');
        $variables['content']['set_packs_list']['nodes']['#grid_cell_attributes'] = array(
          'class' => array('u-xl-size2of3', 'u-ie-size2of3')
        );

        // Wrap each pack in grid cell
        foreach ($nodes as $key => &$node) {
          $node['#theme_wrappers'] = array('minima_grid_cell');
          $node['#grid_cell_attributes'] = array(
            'class' => array('u-xl-size1of2', 'u-lg-size1of2', 'u-ie-size1of2')
          );
        }
        $nodes['#theme_wrappers'] = array('minima_grid');
        $nodes['#grid_attributes'] = array(
          'class' => array('Grid--space Grid--alignCenter'),
        );

        $nodes = $variables['content']['set_packs_list']['nodes']['content'];
      }
    break;

    // Wrap set packs/location info set pack items in grid cells
    case 'set_packs_list_location_info':
      if (is_array($variables['content'])) {
        $nodes = &$variables['content']['set_packs_list_location_info']['nodes']['content'];
        // Wrap each pack in grid cell
        foreach ($nodes as $key => &$node) {
          $node['#theme_wrappers'] = array('minima_grid_cell');
          $node['#grid_cell_attributes'] = array(
            'class' => array('u-xl-size1of2', 'u-lg-size1of2', 'u-ie-size1of2')
          );
        }
        $nodes['#theme_wrappers'] = array('minima_grid');
        $nodes['#grid_attributes'] = array(
          'class' => array('Grid--space'),
        );
      }
    break;
  }
}

/**
 * Implements hook_form_FORM_ID_alter
 */
function city_form_gk_members_signup_cta_form_alter(&$form, &$form_state) {
  $form['title']['#markup'] = t('Stay in the loop');
  $form['title']['#attributes']['class'][] = 'Icon Icon--mail';
}
