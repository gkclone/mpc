<?php if ($content): ?>
  <div<?php print $attributes; ?>>
    <?php if ($display_title): ?>
      <h2 class="Node-title">
        <?php print $title; ?>
      </h2>
    <?php endif; ?>
    <div class="Node-date">
      <?php print render($field_event_date); ?>
    </div>
    <div<?php print $content_attributes; ?>>
      <?php if (!empty($links)): ?>
        <?php print $links; ?>
      <?php endif; ?>
    </div>
  </div>
<?php endif; ?>
