<?php if ($content): ?>
  <div<?php print $attributes; ?>>
    <?php print $file_download_link; ?>
  	<?php print render($links) ?>
  </div>
<?php endif; ?>
