<?php

/**
 * Implements hook_minima_config_settings().
 */
function country_minima_config_settings() {
  $color_scheme_options = array(
    'header_background' => array(
      'title' => 'Header Background',
      'variable' => 'header-background',
    ),
    'body_background' => array(
      'title' => 'Body Background',
      'variable' => 'body-background',
    ),
    'offcanvas_handle_color' => array(
      'title' => 'Mobile Menu Icon Color',
      'variable' => 'offcanvas-handle-color',
    ),

    // Style one
    'color_one_style_one' => array(
      'title' => 'Background Color, Style One',
      'variable' => 'config--color-one--style-one',
    ),
    'color_two_style_one' => array(
      'title' => 'Text Color, Style One',
      'variable' => 'config--color-two--style-one',
    ),
    'color_three_style_one' => array(
      'title' => 'Hover Color/Headings Color, Style One',
      'variable' => 'config--color-three--style-one',
    ),
    'color_four_style_one' => array(
      'title' => 'Accent Color, Style One',
      'variable' => 'config--color-four--style-one',
    ),

    // Style two
    'color_one_style_two' => array(
      'title' => 'Background Color, Style Two',
      'variable' => 'config--color-one--style-two',
    ),
    'color_two_style_two' => array(
      'title' => 'Text Color 1, Style Two',
      'variable' => 'config--color-two--style-two',
    ),
    'color_three_style_two' => array(
      'title' => 'Text Color 2, Style Two',
      'variable' => 'config--color-three--style-two',
    ),
    'color_four_style_two' => array(
      'title' => 'Headings Color, Style Two',
      'variable' => 'config--color-four--style-two',
    ),
    'color_five_style_two' => array(
      'title' => 'Accent Color, Style Two',
      'variable' => 'config--color-five--style-two',
    ),

    // Style three
    'color_one_style_three' => array(
      'title' => 'Background Color 1, Style Three',
      'variable' => 'config--color-one--style-three',
    ),
    'color_two_style_three' => array(
      'title' => 'Background Color 2, Style Three',
      'variable' => 'config--color-two--style-three',
    ),
    'color_three_style_three' => array(
      'title' => 'Text Color, Style Three',
      'variable' => 'config--color-three--style-three',
    ),
    'color_four_style_three' => array(
      'title' => 'Hover Color/Headings Color, Style Three',
      'variable' => 'config--color-four--style-three',
    ),

    // Style four
    'color_one_style_four' => array(
      'title' => 'Background Color, Style Four',
      'variable' => 'config--color-one--style-four',
    ),
    'color_two_style_four' => array(
      'title' => 'Text Color 1, Style Four',
      'variable' => 'config--color-two--style-four',
    ),
    'color_three_style_four' => array(
      'title' => 'Text Color 2, Style Four',
      'variable' => 'config--color-three--style-four',
    ),
    'color_four_style_four' => array(
      'title' => 'Headings Color, Style Four',
      'variable' => 'config--color-four--style-four',
    ),
    'color_five_style_four' => array(
      'title' => 'Accent Color, Style Four',
      'variable' => 'config--color-five--style-four',
    ),

    // Style five
    'color_one_style_five' => array(
      'title' => 'Background Color, Style Five',
      'variable' => 'config--color-one--style-five',
    ),
    'color_two_style_five' => array(
      'title' => 'Text Color, Style Five',
      'variable' => 'config--color-two--style-five',
    ),
    'color_three_style_five' => array(
      'title' => 'Hover Color/Headings Color, Style Five',
      'variable' => 'config--color-three--style-five',
    ),
    'color_four_style_five' => array(
      'title' => 'Accent Color, Style Five',
      'variable' => 'config--color-four--style-five',
    ),

    // Style six
    'color_one_style_six' => array(
      'title' => 'Background Color, Style Six',
      'variable' => 'config--color-one--style-six',
    ),
    'color_two_style_six' => array(
      'title' => 'Text Color 1, Style Six',
      'variable' => 'config--color-two--style-six',
    ),
    'color_three_style_six' => array(
      'title' => 'Text Color 2, Style Six',
      'variable' => 'config--color-three--style-six',
    ),
    'color_four_style_six' => array(
      'title' => 'Headings Color, Style Six',
      'variable' => 'config--color-four--style-six',
    ),
    'color_five_style_six' => array(
      'title' => 'Accent Color, Style Six',
      'variable' => 'config--color-five--style-six',
    ),

    // Style seven
    'color_one_style_seven' => array(
      'title' => 'Background Color, Style Seven',
      'variable' => 'config--color-one--style-seven',
    ),
    'color_two_style_seven' => array(
      'title' => 'Text Color, Style Seven',
      'variable' => 'config--color-two--style-seven',
    ),
    'color_three_style_seven' => array(
      'title' => 'Headings Color, Style Seven',
      'variable' => 'config--color-three--style-seven',
    ),
    'color_four_style_seven' => array(
      'title' => 'Accent Color, Style Seven',
      'variable' => 'config--color-four--style-seven',
    ),

    // Style eight
    'color_one_style_eight' => array(
      'title' => 'Background Color, Style Eight',
      'variable' => 'config--color-one--style-eight',
    ),
    'color_two_style_eight' => array(
      'title' => 'Text Color 1, Style Eight',
      'variable' => 'config--color-two--style-eight',
    ),
    'color_three_style_eight' => array(
      'title' => 'Text Color 2, Style Eight',
      'variable' => 'config--color-three--style-eight',
    ),
    'color_four_style_eight' => array(
      'title' => 'Headings Color, Style Eight',
      'variable' => 'config--color-four--style-eight',
    ),
    'color_five_style_eight' => array(
      'title' => 'Accent Color, Style Eight',
      'variable' => 'config--color-five--style-eight',
    ),

    // Style nine
    'color_one_style_nine' => array(
      'title' => 'Background Color, Style Nine',
      'variable' => 'config--color-one--style-nine',
    ),
    'color_two_style_nine' => array(
      'title' => 'Text Color, Style Nine',
      'variable' => 'config--color-two--style-nine',
    ),
    'color_three_style_nine' => array(
      'title' => 'Accent Color, Style Nine',
      'variable' => 'config--color-three--style-nine',
    ),
  );

  $available_colors = array(
    'color_primary_lighter' => array(
      'title' => 'Color Primary Lighter',
      'variable' => '@color-primary-lighter',
    ),
    'color_primary_light' => array(
      'title' => 'Color Primary Light',
      'variable' => '@color-primary-light',
    ),
    'color_primary' => array(
      'title' => 'Color Primary',
      'variable' => '@color-primary',
    ),
    'color_primary_dark' => array(
      'title' => 'Color Primary Dark',
      'variable' => '@color-primary-dark',
    ),
    'color_primary_darker' => array(
      'title' => 'Color Primary Darker',
      'variable' => '@color-primary-darker',
    ),
    'color_secondary_lighter' => array(
      'title' => 'Color Secondary Lighter',
      'variable' => '@color-secondary-lighter',
    ),
    'color_secondary_light' => array(
      'title' => 'Color Secondary Light',
      'variable' => '@color-secondary-light',
    ),
    'color_secondary' => array(
      'title' => 'Color Secondary',
      'variable' => '@color-secondary',
    ),
    'color_secondary_dark' => array(
      'title' => 'Color Secondary Dark',
      'variable' => '@color-secondary-dark',
    ),
    'color_secondary_darker' => array(
      'title' => 'Color Secondary Darker',
      'variable' => '@color-secondary-darker',
    ),
    'color_neutral_lightest' => array(
      'title' => 'Color Neutral Lightest',
      'variable' => '@color-neutral-lightest',
    ),
    'color_neutral_lighter' => array(
      'title' => 'Color Neutral Lighter',
      'variable' => '@color-neutral-lighter',
    ),
    'color_neutral_light' => array(
      'title' => 'Color Neutral Light',
      'variable' => '@color-neutral-light',
    ),
    'color_neutral' => array(
      'title' => 'Color Neutral',
      'variable' => '@color-neutral',
    ),
    'color_neutral_dark' => array(
      'title' => 'Color Neutral Dark',
      'variable' => '@color-neutral-dark',
    ),
    'color_neutral_darker' => array(
      'title' => 'Color Neutral Darker',
      'variable' => '@color-neutral-darker',
    ),
    'color_neutral_darkest' => array(
      'title' => 'Color Neutral Darkest',
      'variable' => '@color-neutral-darkest',
    ),
  );

  foreach ($color_scheme_options as $color_scheme_key => $color_scheme_option) {
    $minima_settings[$color_scheme_key] = array(
      'title' => $color_scheme_option['title'],
    );

    $minima_settings[$color_scheme_key]['options'] = array();

    foreach ($available_colors as $available_color_key => $available_color) {
      $minima_settings[$color_scheme_key]['options'][$available_color_key] = array();
      $minima_settings[$color_scheme_key]['options'][$available_color_key]['title'] = $available_color['title'];
      $minima_settings[$color_scheme_key]['options'][$available_color_key]['variables'][$color_scheme_option['variable']] = $available_color['variable'];
    }
  }

  return $minima_settings;
}

/**
 * Preprocess variables for theme panels_pane.
 */
function country_preprocess_panels_pane(&$variables) {
  switch ($variables['pane']->type) {
    case 'rendered_node_location_info':
    case 'rendered_node_booking_widget':
    case 'location_node_body_location_info':
      $variables['container_attributes_array']['class'][] = 'Container--secondary';
    break;

    // Wrapping booking widget/location info pane into two grid cells
    case 'bookings_widget_location_info':
      $variables['container_attributes_array']['class'][] = 'Container--banner';

      $variables['content']['grid']['grid_cell']['#grid_cell_attributes'] = array(
        'class' => array('u-xl-size2of3', 'u-ie-size2of3'),
      );

      $variables['content']['grid']['grid_cell']['box']['grid']['#grid_attributes'] = array(
        'class' => array('Grid--space'),
      );

      $variables['content']['grid']['grid_cell']['box']['grid']['location_info']['#grid_cell_attributes'] = array(
        'class' => array('u-xl-size1of2', 'u-lg-size1of2', 'u-ie-size1of2'),
      );

      $variables['content']['grid']['grid_cell']['box']['grid']['booking_widget']['#grid_cell_attributes'] = array(
        'class' => array('u-xl-size1of2', 'u-lg-size1of2', 'u-ie-size1of2'),
      );

      if (!empty($variables['content']['grid']['background_image'])) {
        $url = file_create_url($variables['content']['grid']['background_image']);
        $variables['container_attributes_array']['style'][] = 'background-image: url(' . $url . ');';
      }
    break;

    // Wrapping events teaser list in grid cells
    case 'gk_events_list':
      if ($variables['is_front']) {
        $variables['container_attributes_array']['class'][] = 'Container--tertiary';
        $nodes = $variables['content'];
        $variables['content'] = array(
          '#theme_wrappers' => array('minima_grid'),
          'content' => $nodes,
        );
        // Wrap each node in the events list in a grid cell, if the nodes exist
        if (is_array($variables['content']['content'])) {
          foreach ($variables['content']['content'] as $key => $node) {
            $variables['content']['content'][$key]['#theme_wrappers'] = array('minima_grid_cell');
            $variables['content']['content'][$key]['#grid_cell_attributes'] = array(
              'class' => array('u-xl-size1of3', 'u-lg-size1of3', 'u-lg-size1of3'),
            );
          }
        }
      }

      // Add Container--tertiary class for sticky events list
      if ($variables['pane']->configuration['view_mode'] == 'sticky') {
        $variables['container_attributes_array']['class'][] = 'Container--primary';
      }
    break;

    // Add quaternary class to the the secret dj feed wrapper
    case 'gk_secret_dj_now_playing':
      $variables['container_attributes_array']['class'][] = 'Container--quaternary';
      $secret_dj = &$variables['content']['box_inner']['secret_dj'];

      array_unshift($secret_dj['#theme_wrappers'], 'minima_grid');
      $secret_dj['#grid_attributes'] = array('class' => array('Grid--spaceHorizontal'));

      $secret_dj['song_info']['#theme_wrappers'][] = 'minima_grid_cell';
      $secret_dj['song_info']['#grid_cell_attributes'] = array(
        'class' => array('u-xl-size2of5', 'u-lg-size2of5'),
      );

      $secret_dj['content']['#theme_wrappers'][] = 'minima_grid_cell';
      $secret_dj['content']['#grid_cell_attributes'] = array(
        'class' => array('u-xl-size3of5', 'u-lg-size3of5')
      );

      if ($variables['title']) {
        $variables['content']['title'] = array(
          '#theme' => 'html_tag',
          '#tag' => 'h2',
          '#attributes' => array(
            'class' => array('Box-title'),
          ),
          '#value' => $variables['title'],
          '#weight' => -10
        );
        $variables['title'] = '';
      }
    break;

    // Wrapping set packs in grid cells
    case 'set_packs_list':
      if (is_array($variables['content'])) {
        $nodes = &$variables['content']['set_packs_list']['nodes']['content'];

        // Center grid cells
        $variables['content']['set_packs_list']['#grid_attributes']['class'] = 'Grid--alignCenter';
        $variables['content']['set_packs_list']['nodes']['#theme_wrappers'] = array('minima_grid_cell');
        $variables['content']['set_packs_list']['nodes']['#grid_cell_attributes'] = array(
          'class' => array('u-xl-size2of3', 'u-ie-size2of3')
        );

        // Wrap each pack in grid cell
        foreach ($nodes as $key => &$node) {
          $node['#theme_wrappers'] = array('minima_grid_cell');
          $node['#grid_cell_attributes'] = array(
            'class' => array('u-xl-size1of2', 'u-lg-size1of2', 'u-ie-size1of2')
          );
        }
        $nodes['#theme_wrappers'] = array('minima_grid');
        $nodes['#grid_attributes'] = array(
          'class' => array('Grid--space Grid--alignCenter'),
        );

        $nodes = $variables['content']['set_packs_list']['nodes']['content'];
      }
    break;

    // Wrapping location info and set packs lists in two grid cells
    // and wrapping set packs into two grid cells within that
    case 'set_packs_list_location_info':
      if (is_array($variables['content'])) {
        $variables['container_attributes_array']['class'][] = 'Container--primary';
        $nodes = &$variables['content']['set_packs_list_location_info']['nodes']['content'];

        foreach ($nodes as $key => &$node) {
          $node['#theme_wrappers'] = array('minima_grid_cell');
          $node['#grid_cell_attributes'] = array(
            'class' => array('u-xl-size1of2', 'u-lg-size1of2', 'u-ie-size1of2')
          );
        }

        $variables['content']['set_packs_list_location_info']['nodes']['content']['#theme_wrappers'] = array('minima_grid');

        $variables['content']['set_packs_list_location_info']['nodes']['content']['#grid_attributes'] = array(
          'class' => array('Grid--space'),
        );

        $nodes = $variables['content']['set_packs_list_location_info']['nodes']['content'];

        $variables['content']['set_packs_list_location_info']['nodes']['content'] = array(
          '#theme_wrappers' => array('container'),
          '#attributes' => array(
            'class' => array('Box', 'Box--setPacks'),
          ),
          'title' => array(
            '#value' => t('See our set packs'),
            '#theme' => 'html_tag',
            '#tag' => 'h2',
            '#attributes' => array(
              'class' => 'Box-title',
            ),
          ),
          'content' => $nodes,
        );
      }
    break;

    // Adding Container--webform class to webform containers
    case 'block':
      if (substr($variables['pane']->subtype, 0, 8) === 'webform-') {
        $variables['container_attributes_array']['class'][] = 'Container--webform';
      }
    break;
  }
}
