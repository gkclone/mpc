<?php

function metropolitan_pub_company_minima_config_settings() {
  // Get a list of webfonts from Google APIs.
  $google_api_key = 'AIzaSyAcxA2jN7-n2PmCuzecxjXnS-4q8GRhb_E';
  $webfonts_url = 'https://www.googleapis.com/webfonts/v1/webfonts?key=' . $google_api_key;

  $fonts = drupal_http_request($webfonts_url);
  $fonts = drupal_json_decode($fonts->data);

  $font_1_options = array();
  $font_2_options = array();

  // Compile an array of font options to be returned as the heading and base
  // font settings.
  foreach ($fonts['items'] as $i => $font) {
    $font_stack = '"' . $font['family'] . '", ' . $font['category'];
    $font_variants = implode(',', $font['variants']);

    // The equals sign in the URL has to be encoded so that the arguement can be
    // passed successfully to the LESS compiler on the command line. We'll use
    // "%3d" since it's the standard URL encoded version of "=". Important to
    // note that this could be removed if bash sorts it's fucking life out.
    $font_file = '"http://fonts.googleapis.com/css?family%3d' . urlencode($font['family']) . ':' . $font_variants . '&.css"';

    $font_1_options[$font['family']] = array(
      'title' => $font['family'],
      'variables' => array(
        'font-one-file' => $font_file,
        'font-one-family' => $font_stack,
      ),
    );

    $font_2_options[$font['family']] = array(
      'title' => $font['family'],
      'variables' => array(
        'font-two-file' => $font_file,
        'font-two-family' => $font_stack,
      ),
    );
  }

  $minima_settings = array(
    'font_1' => array(
      'title' => 'Font 1',
      'options' => $font_1_options,
    ),
    'font_2' => array(
      'title' => 'Font 2',
      'options' => $font_2_options,
    ),
    'heading_font' => array(
      'title' => 'Heading Font',
      'options' => array(
        'font_1' => array(
          'title' => 'Font 1',
          'variables' => array(
            'config--heading-font-family' => '@font-one-family',
          ),
        ),
        'font_2' => array(
          'title' => 'Font 2',
          'variables' => array(
            'config--heading-font-family' => '@font-two-family',
          ),
        ),
      ),
    ),
    'base_font' => array(
      'title' => 'Base Font',
      'options' => array(
        'font_1' => array(
          'title' => 'Font 1',
          'variables' => array(
            'config--base-font-family' => '@font-one-family',
          ),
        ),
        'font_2' => array(
          'title' => 'Font 2',
          'variables' => array(
            'config--base-font-family' => '@font-two-family',
          ),
        ),
      ),
    ),
    'navigation_font' => array(
      'title' => 'Navigation Font',
      'options' => array(
        'font_1' => array(
          'title' => 'Font 1',
          'variables' => array(
            'config--navigation-font-family' => '@font-one-family',
          ),
        ),
        'font_2' => array(
          'title' => 'Font 2',
          'variables' => array(
            'config--navigation-font-family' => '@font-two-family',
          ),
        ),
      ),
    ),
    'color_primary' => array(
      'title' => 'Color Primary',
      'variable' => 'config--color-primary',
    ),
    'color_secondary' => array(
      'title' => 'Color Secondary',
      'variable' => 'config--color-secondary',
    ),
  );

  return $minima_settings;
}

/**
 * Implements hook_theme_registry_alter().
 */
function metropolitan_pub_company_theme_registry_alter(&$theme_registry) {
  $path = drupal_get_path('theme', 'metropolitan_pub_company') . '/templates';

  // Override the template for the pane_content_header theme.
  $theme_registry['pane_content_header']['path'] = $path;

  // Override the template for the OpenTable widget.
  $theme_registry['gk_table_booking_opentable_widget']['path'] = $path;
}

/**
 * Preprocess variables for the html template.
 */
function metropolitan_pub_company_preprocess_html(&$variables, $hook) {
  // Add JS to the page.
  drupal_add_js(libraries_get_path('minima') . '/dist/js/minima.offcanvas.min.js');
  drupal_add_js(drupal_get_path('theme', 'metropolitan_pub_company') . '/js/modernizr.js');
}

/**
 * Preprocess variables for theme minima_site_layout.
 */
function metropolitan_pub_company_preprocess_minima_site_layout(&$variables) {
  // Add classes to the markup so that the page behaves responsively.
  $variables['minima_layout']['header_container']['#grid_attributes']['class'] = array('Grid--spaceHorizontal', 'Grid--alignMiddle');
  $variables['minima_layout']['header_container']['header']['#grid_cell_attributes']['class'] = array('u-lg-size3of4', 'u-md-size3of4', 'u-sm-size3of4', 'u-xs-size3of4');
}

/**
 * Process variables for the html template.
 */
function metropolitan_pub_company_process_html(&$variables, $hook) {
  // Add CSS/JS to the page.
  global $theme;

  if ($theme == 'metropolitan_pub_company') {
    drupal_add_css(libraries_get_path('minima') . '/dist/css/minima.min.css');
    $variables['styles'] = drupal_get_css();
  }
  else {
    $less_settings = less_get_settings($theme);
    $less_settings['variables']['minima-path'] = '~"' . libraries_get_path('minima') . '"';
    $less_settings['variables']['mpc-style-guide-path'] = '~"' . libraries_get_path('style_guide_metropolitan_pub_company') . '"';

    $style_guide_path = libraries_get_path('style_guide_' . $theme);

    drupal_add_css($style_guide_path . '/src/less/app.less', array(
      'less' => $less_settings,
    ));

    drupal_add_css($style_guide_path . '/src/less/ie.less', array(
      'browsers' => array(
        'IE' => 'lt IE 9',
        '!IE' => FALSE,
      ),
      'less' => $less_settings,
    ));

    $variables['styles'] = drupal_get_css();

    // Add the metro styleguide
    drupal_add_js(libraries_get_path('style_guide_metropolitan_pub_company') . '/src/js/styleguide.js');

    // Add the current themes styleguide
    drupal_add_js($style_guide_path . '/src/js/styleguide.js');
    drupal_add_js(drupal_get_path('theme', 'metropolitan_pub_company') . '/js/metropolitan_pub_company.js');
    $variables['scripts'] = drupal_get_js();
  }
}

/**
 * Preprocess variables for theme panels_pane.
 */
function metropolitan_pub_company_preprocess_panels_pane(&$variables) {
  $pane = $variables['pane'];

  // Output panes in the 'contained' template if they are intended to be full
  // width (e.g. any pane in a 1 column layout, or in top/bottom regions, etc.).
  $contained_regions = array('top' => 1, 'bottom' => 1);

  if ($variables['display']->layout == '1_column' || isset($contained_regions[$pane->panel])) {
    $variables['theme_hook_suggestions'][] = 'box__contained';

    $variables['container_attributes_array']['class'][] = 'Container';
    $variables['container_attributes_array']['class'][] = 'Container--box';
  }

  // Set various classes for panes
  switch ($variables['pane']->type) {
    case 'promotion':
      // Wrap text & image banner promotions in a grid cell
      if ($variables['pane']->configuration['view_mode'] === 'banner') {
        foreach ($variables['content']['promotion'] as &$promotion) {
          if ($promotion['#entity']->type === 'text_image') {
            $promotion['#theme_wrappers'] = array('minima_grid_cell', 'minima_grid');
            $promotion['#grid_cell_attributes'] = array(
              'class' => array('u-xl-size2of5', 'u-ie-size2of5', 'u-xl-push3of5'),
            );
          }
        }
      }
    break;

    // Adding Container--primary class to the promotion groups container
    // Adding Container--gallery class to the gallery slideshow container
    case 'promotion_group':
      $promotion_group = reset($variables['content']['promotion_group']);
      $format = reset($promotion_group['field_promotion_group_format']['#items']);
      $format = $format['value'];
      if ($format == 'default') {
        $variables['container_attributes_array']['class'][] = 'Container--primary';
      }
      elseif ($format == 'slideshow_thumbnails') {
        $variables['container_attributes_array']['class'][] = 'Container--gallery';
      }
    break;

    // Add container classes for the signup form
    case 'gk_members_signup_cta_form':
      $variables['container_attributes_array']['class'][] = 'Container--quaternary';
      $variables['container_attributes_array']['class'][] = 'Container--signUp';
    break;

    // Remove grid cell wrappers from signup form
    case 'signup_cta_form_location_info':
      unset($variables['content']['signup_cta_form']['title']['#theme_wrappers']);
      unset($variables['content']['signup_cta_form']['mail']['#theme_wrappers']);
      unset($variables['content']['signup_cta_form']['submit']['#theme_wrappers']);
      $variables['content']['signup_cta_form']['#theme_wrappers'] = array('form');
    break;

    // Set location info into three panes
    case 'gk_locations_info':
      if ($variables['pane']->panel == 'primary') {
        $location_infos = $variables['content'];
        $location_infos_count = count($location_infos);
        $row_max = 3;

        if ($location_infos_count > 1) {
          if ($location_infos_count <= $row_max) {
            $grid_cell_classes = array(
              'u-xl-size1of' . $location_infos_count,
              'u-lg-size1of' . $location_infos_count,
              'u-ie-size1of' . $location_infos_count,
            );
          }
          else {
            $grid_cell_classes = array(
              'u-xl-size1of' . $row_max,
              'u-lg-size1of' . $row_max,
              'u-ie-size1of' . $row_max,
            );
          }

          foreach ($location_infos as $key => $location_info) {
            $location_infos[$key]['#theme_wrappers'] = array('container', 'minima_grid_cell');
            $location_infos[$key]['#grid_cell_attributes'] = array(
              'class' => $grid_cell_classes,
            );
          }

          $variables['content'] = array(
            'location_info' => array(
              '#theme_wrappers' => array('minima_grid'),
              '#grid_attributes' => array(
                'class' => array('Grid--space'),
              ),
              'content' => $location_infos,
            ),
          );
        }
      }
    break;

    // Add class to location map container
    case 'gk_locations_map':
      $variables['container_attributes_array']['class'][] = 'Container--locationsMap';
    break;

    // case 'mpc_core_location_info_image':
    case 'location_info_image':
      $variables['container_attributes_array']['class'][] = 'Container--locationInfoImage';
      $variables['container_attributes_array']['class'][] = 'Container--tertiary';

      if (!empty($variables['content']['image'])) {
        $image_url = file_create_url($variables['content']['image']);
        $variables['container_attributes_array']['style'] = "background-image: url($image_url)";
      }
    break;
  }

  // Hide the title for the main navigation pane and apply OffCanvas data
  // attributes.
  if ($pane->panel == 'navigation') {
    if ($pane->subtype == 'domain_menu_block-main_menu') {
      $variables['attributes_array']['data-offcanvas-panel'][] = 'primary';
    }
  }

  // Wrap boxes in the footer region in cells.
  elseif ($pane->panel == 'footer') {
    $grid_cell_classes = '';

    switch ($pane->subtype) {
      case 'domain_menu_block-footer_menu':
        $grid_cell_classes = 'u-xl-size1of3 u-ie-size1of3';
      break;

      case 'gk_locations_info':
        $grid_cell_classes = 'u-xl-size1of3 u-ie-size1of3';
      break;

      case 'signup_cta_form_location_info':
        $grid_cell_classes = 'u-xl-size1of3 u-ie-size1of3';

        // Set custom text for the social network links.
        if (!empty($variables['content']['location_info']['social']['content']['content'])) {
          $links = &$variables['content']['location_info']['social']['content']['content'];
          $current_location = gk_locations_get_current_location();

          foreach ($links as $name => &$link) {
            switch ($name) {
              case 'instagram':
                if ($current_location) {
                  $items = field_get_items('node', $current_location, 'field_location_social_instagram');
                  $link['#text'] = '/' . $items[0]['value'];
                }
              break;

              case 'facebook':
                if ($current_location) {
                  $items = field_get_items('node', $current_location, 'field_location_social_facebook');
                  $link['#text'] = '/' . $items[0]['value'];
                }
              break;

              case 'twitter':
                if ($current_location) {
                  $items = field_get_items('node', $current_location, 'field_location_social_twitter');
                  $link['#text'] = '@' . $items[0]['value'];
                }
              break;

              case 'tripadvisor':
                $link['#text'] = t('Rate us on TripAdvisor');
              break;
            }
          }
        }
      break;
    }

    if (!empty($grid_cell_classes)) {
      $variables['box_prefix'] = '<div class="Grid-cell ' . $grid_cell_classes . '">';
      $variables['box_suffix'] = '</div>';
    }
  }
}

/**
 * Process variables for theme panels_pane.
 */
function metropolitan_pub_company_process_panels_pane(&$variables) {
  if (!empty($variables['container_attributes_array'])) {
    $variables['container_attributes'] = drupal_attributes($variables['container_attributes_array']);
  }
}

/**
 * Preprocess variables for nodes
 */
function metropolitan_pub_company_preprocess_node(&$variables) {
  $variables['theme_hook_suggestions'][] = 'node__' . $variables['type'] . '__' . $variables['view_mode'];

  if ($variables['type'] == 'event') {
    $variables['attributes_array']['class'] = array(
      'Node',
      'Node--event',
      'Node--' . minima_camelcase($variables['view_mode']),
    );

    if ($variables['view_mode'] == 'full') {
      $variables['display_title'] = TRUE;
    }
    else {
      $node_title_stripped = strip_tags($variables['node']->title);
      $variables['content']['links']['node']['#links']['node-readmore'] = array(
        'title' => t('Read more<span class="is-hiddenVisually"> about @title</span>', array('@title' => $node_title_stripped)),
        'href' => 'node/' . $variables['node']->nid,
        'html' => TRUE,
        'attributes' => array('rel' => 'tag', 'title' => $node_title_stripped),
      );

      if ($variables['view_mode'] == 'sticky') {
        $variables['title_tag'] = 'h2';
        $variables['title_attributes_array']['class'][] = 'Node-title';

        if ($image = reset($variables['field_event_image'])) {
          $variables['attributes_array']['style'] = array(
            'background-image: url(' . file_create_url($image['uri']) . ');',
          );
        }
      }
    }
  }
}

/**
 * Preprocess variables for entities
 */
function metropolitan_pub_company_preprocess_entity(&$variables) {
  switch ($variables['entity_type']) {
    // Prmotion groups
    // Wrap promotions in grid cells
    case 'promotion_group':
      $format = reset($variables['field_promotion_group_format']);
      $format = $format['value'];
      if ($format == 'default') {
        $promotions = $variables['promotions'];
        $variables['promotions'] = array(
          'promotions' => $promotions,
        );

        // Wrap each promotion in a grid cell
        foreach ($variables['promotions']['promotions'] as $key => $promotion) {
          $variables['promotions']['promotions'][$key]['#theme_wrappers'] = array('minima_grid_cell');
          $variables['promotions']['promotions'][$key]['#grid_cell_attributes'] = array(
            'class' => array('u-xl-size1of3', 'u-lg-size1of3', 'u-ie-size1of3'),
          );
        }

        // Wrap all promotions in a grid
        $variables['promotions']['promotions']['#theme_wrappers'] = array('minima_grid');
        $variables['promotions']['promotions']['#grid_attributes'] = array(
          'class' => array('Grid--space'),
        );
      }
    break;

    // GK Menus
    // Add box classes, move meun title into H1 tag
    case 'gk_menu':
      if (isset($variables['content']['description'])) {
        $menu_description = $variables['content']['description'];
        $menu_description['#attributes']['class'] = array('Box-content');
        $menu_description['#weight'] = 10;
      }

      $variables['content']['description'] = array(
        '#theme_wrappers' => array('container'),
        '#attributes' => array(
          'class' => array('Box', 'Box--gkMenuIntro'),
        ),
        '#weight' => -100,
        'title' => array(
          '#attributes' => array(
            'class' => array('Box-title'),
          ),
          '#tag' => 'h1',
          '#theme' => 'html_tag',
          '#value' => $variables['title'] ? $variables['title'] : '',
          '#weight' => -100,
        ),
        'content' => isset($menu_description) ? $menu_description : array(),
      );

      // Availability.
      $pane_conf = array('override_title' => FALSE);
      ctools_plugin_load_function('ctools', 'content_types', 'gk_menus_availability', 'render callback');

      if ($availability = gk_menus_gk_menus_availability_content_type_render('gk_menus_availability', $pane_conf, array(), array())) {
        $variables['content']['availability'] = array(
          '#weight' => -95,
          '#theme_wrappers' => array('container'),
          '#attributes' => array(
            'class' => array('Box', 'Box--gkMenuAvailability'),
          ),
	  'title' => array(
	    '#theme' => 'html_tag',
	    '#tag' => 'h2',
	    '#value' => 'Times',
	    '#attributes' => array(
	      'class' => array('Box-title'),
	    ),
	  ),
          'content' => array(
            '#theme_wrappers' => array('container'),
            '#attributes' => array(
              'class' => array('Box-content'),
            ),
            'content' => $availability->content,
          ),
        );
      }

      // Menu PDF link.
      $meta_data = gk_menus_get_active_meta_data($variables['gk_menu']);

      if (!empty($meta_data['field_menu_pdf'])) {
        $uri = entity_uri('gk_menu', $variables['gk_menu']);

        $variables['content']['description']['content']['pdf_link'] = array(
          '#theme_wrappers' => array('container'),
          '#attributes' => array(
            'class' => array('Box--gkMenuPDFLink'),
          ),
          '#markup' => l(t('Download a PDF ') . '<span class="Icon Icon--pdf"></span>', $uri['path'] . '/pdf', array(
            'html' => TRUE,
          )),
        );

        if (!empty($variables['content']['description']['content']['#markup'])) {
          $content = $variables['content']['description']['content']['#markup'];
          unset($variables['content']['description']['content']['#markup']);

          $variables['content']['description']['content']['content'] = array(
            '#markup' => $content,
          );
        }
      }
    break;
  }
}

/**
 * Implements hook_gk_menus_build_sections_alter().
 */
function metropolitan_pub_company_gk_menus_build_sections_alter(&$build, $depth) {
  foreach ($build as $key => $section) {
    // If there's a section header or footer move it inside the section content.
    if (isset($build[$key]['inner']['description'])) {
      $box_header = $build[$key]['inner']['description'];
      unset($build[$key]['inner']['description']);
      $build[$key]['inner']['items'][] = $box_header;
    }

    if (isset($build[$key]['inner']['footer'])) {
      $box_footer = $build[$key]['inner']['footer'];
      unset($build[$key]['inner']['footer']);
      $build[$key]['inner']['items'][] = $box_footer;
    }
  }
}

/**
 * Preprocess variables for theme minima_page_layout.
 */
function metropolitan_pub_company_preprocess_minima_page_layout(&$variables) {
  if ($variables['display']->layout === '1_column') {
    $variables['minima_layout'] = array(
      'primary' => array(
        '#markup' => $variables['content']['primary'],
      ),
    );
  }
}

/**
 * Preprocess variables for the htmlmail template.
 */
function metropolitan_pub_company_preprocess_htmlmail(&$variables) {
  // Check if there is an htmlmail suitable logo for the current domain.
  $domain = domain_get_domain();
  $domain_theme = domain_theme_lookup($domain['domain_id']);
  $domain_theme_settings = unserialize($domain_theme['settings']);

  if (!empty($domain_theme_settings['logo_path'])) {
    $htmlmail_logo_path = $domain_theme_settings['logo_path'];

    if (file_exists($htmlmail_logo_path . '.htmlmail.png')) {
      $htmlmail_logo_path .= '.htmlmail.png';
    }

    $variables['logo'] = theme('image', array(
      'path' => $htmlmail_logo_path,
    ));
  }

  // Add minima_config LESS variables to $variables to be picked up by gk_mail.
  // Also add paths to the MPC and currently active style guides.
  $domain_theme_name = $domain_theme['theme'];

  $mpc_style_guide_path = libraries_get_path('style_guide_metropolitan_pub_company');
  $style_guide_path = libraries_get_path('style_guide_' . $domain_theme_name);

  $variables['less_variables'] = $domain_theme_settings['less'][$domain_theme_name] + array(
    'mpc-style-guide-path' => '~"' . $mpc_style_guide_path . '"',
    'style-guide-path' => '~"' . $style_guide_path . '"',
  );
}

/**
 * Implements hook_form_FORM_ID_alter
 */
function metropolitan_pub_company_form_gk_members_signup_cta_form_alter(&$form, &$form_state) {
  // Add the class Form--membersSignUp to the form
  $form['#attributes']['class'][] = 'Form--membersSignUp';

  // Make sure the form items are wrapped in a grid
  $form['#theme_wrappers'] = array('minima_grid', 'form');
  $form['#grid_attributes']['class'] = array('Grid--spaceHorizontal');

  $form['title'] = array(
    '#attributes' => array(
      'class' => array('Form-title'),
    ),
    '#grid_cell_attributes' => array(
      'class' => array('u-xl-size4of12', 'u-ie-size4of12'),
    ),
    '#markup' => t('Sign up to our newsletter'),
    '#theme_wrappers' => array('container', 'minima_grid_cell'),
    '#weight' => -1,
  );

  $form_items = array(
    'mail',
    'submit',
  );

  foreach ($form_items as $form_item) {
    $form_item_tmp = $form[$form_item];
    $form[$form_item] = array(
      '#theme_wrappers' => array('minima_grid_cell'),
    );
    $form[$form_item][$form_item] = $form_item_tmp;
  }

  // Wrap the mail field and submit button in grid cells
  $form['mail']['#grid_cell_attributes']['class'] = array('u-md-size2of3', 'u-lg-size4of5', 'u-xl-size6of12', 'u-ie-size6of12');
  $form['submit']['#grid_cell_attributes']['class'] = array('u-md-size1of3', 'u-lg-size1of5', 'u-xl-size2of12', 'u-ie-size2of12');
}

/**
 * Implements hook_form_FORM_ID_alter
 */
function metropolitan_pub_company_form_gk_rewards_user_details_form_alter(&$form, &$form_state) {
  // Rearrange name fields into a grid...
  $form['name']['#theme_wrappers'] = array('minima_grid');
  $form['name']['#grid_attributes']['class'] = array('Grid--spaceHorizontal', 'Grid--formItemName');
  unset($form['name']['#type']);

  // ...name fields.
  $form['name']['fields'] = array(
    'label' => $form['name']['label'],
    'title' => $form['name']['title'],
    'first_name' => $form['name']['first_name'],
    'last_name' => $form['name']['last_name'],
  );

  unset($form['name']['label']);
  unset($form['name']['title']);
  unset($form['name']['first_name']);
  unset($form['name']['last_name']);

  // ...name label.
  $form['name']['fields']['label']['#type'] = 'item';
  $form['name']['fields']['label']['#title'] = t('Name');
  $form['name']['fields']['label']['#theme_wrappers'] = array('form_element', 'minima_grid_cell');
  unset($form['name']['fields']['label']['#prefix']);

  // ...title.
  $form['name']['fields']['title']['#theme_wrappers'] = array('form_element', 'minima_grid_cell');
  $form['name']['fields']['title']['#grid_cell_attributes']['class'] = array('u-xl-size4of16', 'u-lg-size4of16', 'u-ie-size4of16');
  $form['name']['fields']['title']['#attributes']['required'] = TRUE;

  // ...first name.
  $form['name']['fields']['first_name']['#theme_wrappers'] = array('form_element', 'minima_grid_cell');
  $form['name']['fields']['first_name']['#grid_cell_attributes']['class'] = array('u-xl-size6of16', 'u-lg-size6of16', 'u-ie-size6of16');
  $form['name']['fields']['first_name']['#attributes']['required'] = TRUE;

  // ...last name.
  $form['name']['fields']['last_name']['#theme_wrappers'] = array('form_element', 'minima_grid_cell');
  $form['name']['fields']['last_name']['#grid_cell_attributes']['class'] = array('u-xl-size6of16', 'u-lg-size6of16', 'u-ie-size6of16');
  $form['name']['fields']['last_name']['#attributes']['required'] = TRUE;

  // Set grid cell classes and attributes for other fields...
  $size_field = array();

  // ...email.
  $form['mail']['#field_grid_cell_attributes']['class'] = $size_field;
  $form['mail']['#attributes']['placeholder'] = 'E.g. yourname@example.com';
  $form['mail']['#attributes']['required'] = TRUE;
  unset($form['confirm_mail']);

  // ...postcode.
  $form['postcode']['#field_grid_cell_attributes']['class'] = $size_field;
  $form['postcode']['#attributes']['placeholder'] = 'E.g. W1U 7EL';
  $form['postcode']['#attributes']['required'] = TRUE;
  $form['postcode']['#attributes']['pattern'] = '[A-Za-z]{1,2}[0-9Rr][0-9A-Za-z]? ?[0-9][ABD-HJLNP-UW-Zabd-hjlnp-uw-z]{2}';

  // ...date of birth.
  $form['date_of_birth']['#field_grid_cell_attributes']['class'] = $size_field;
}

/**
 * Implements hook_form_alter().
 */
function metropolitan_pub_company_form_alter(&$form, &$form_state, $form_id) {
  if (isset($form['#node']) && $form['#node']->type == 'webform') {
    $machine_name = gk_machine_name_lookup_by_entity($form['#node']->nid);
    if ($machine_name == 'mpc_webform_booking_enquiry') {
      $form['submitted']['#theme_wrappers'] = array('minima_grid');
      $form['submitted']['#grid_attributes'] = array(
        'class' => array('Grid--space'),
      );

      if ($form['#node']->title) {
        $form['submitted']['title'] = array(
          '#attributes' => array(
            'class' => 'Form-title',
          ),
          '#tag' => 'h2',
          '#theme' => 'html_tag',
          '#value' => $form['#node']->title,
          '#weight' => -10,
        );
      }

      // Set weights to help stop items being rearranged.
      $form['submitted']['description']['#weight'] = -9;
      $form['submitted']['first_name']['#weight'] = -8;
      $form['submitted']['last_name']['#weight'] = -7;
      $form['submitted']['email']['#weight'] = -6;
      $form['submitted']['enquiry_details']['#weight'] = -5;

      $form['submitted']['title']['#prefix'] = '<div class="Grid-cell u-xl-size1of3 u-ie-size1of3">';
      $form['submitted']['description']['#suffix'] = '</div>';

      $form['submitted']['first_name']['#prefix'] = '<div class="Grid-cell u-xl-size1of3 u-lg-size1of2 u-ie-size1of3">';
      $form['submitted']['email']['#suffix'] = '</div>';

      $form['submitted']['enquiry_details']['#prefix'] = '<div class="Grid-cell u-xl-size1of3 u-lg-size1of2 u-ie-size1of3">';
      $form['submitted']['enquiry_details']['#suffix'] = '</div>';

      $form['actions']['#attributes']['class'][] = 'u-textRight';
    }
  }
}

/**
 * Implements hook_gk_promotion_group_formats_alter().
 */
function metropolitan_pub_company_gk_promotion_group_formats_alter(&$formats) {
  $formats['slideshow_thumbnails']['preprocess functions'] = array('metropolitan_pub_company_gk_promotion_groups_format_slideshow_thumbnails_preprocess');
}

/**
 * Preprocess function for the 'slideshow_thumbnails' promotion group format.
 */
function metropolitan_pub_company_gk_promotion_groups_format_slideshow_thumbnails_preprocess(&$variables) {
  $variables['title_attributes_array']['class'][] = 'is-hiddenVisually';

  // Compile an array of thumbnail images and add them to Drupal.settings
  $thumbnail_images = array();

  foreach ($variables['promotions'] as $promotion) {
    $render = reset($promotion['promotion']);
    $promotion = $render['#entity'];

    if ($image = field_get_items('promotion', $promotion, 'field_promotion_image')) {
      $image = theme('image_style', array(
        'style_name' => 'large',
        'path' => $image[0]['uri'],
      ));

      $count_promotions = count($variables['promotions']);
      $grid_cell_size = $count_promotions <= 6 ? $count_promotions : 6;

      $thumbnail_image = array(
        '#theme_wrappers' => array('minima_grid_cell'),
        '#grid_cell_attributes' => array(
          'class' => array('u-size1of' . $grid_cell_size),
        ),
        '#theme' => 'link',
        '#text' => $image,
        '#path' => '#',
        '#options' => array(
          'html' => TRUE,
          'attributes' => array(),
         ),
      );

      $thumbnail_images[] = drupal_render($thumbnail_image);
    }
  }

  drupal_add_js(array('thumbnail_images' => $thumbnail_images), 'setting');

  // Setup jQuery Cycle plugin.
  $variables['attributes_array'] += array(
    'data-cycle-fx' => 'fade',
    'data-cycle-timeout' => 5000,
    'data-cycle-speed' => 600,
    'data-cycle-slides' => '.Promotion',
    'data-cycle-pager' => '> .PromotionGroup-controls > .PromotionGroup-pager > .Grid',
    'data-cycle-prev' => '> .PromotionGroup-controls > .PromotionGroup-prev',
    'data-cycle-next' => '> .PromotionGroup-controls > .PromotionGroup-next',
    'data-cycle-pager-template' => '',
    'data-cycle-pause-on-hover' => 'true',
    'data-cycle-swipe' => 'true',
  );

  $variables['show_controls'] = count($variables['promotions']) > 1;

  drupal_add_js(libraries_get_path('cycle2') . '/jquery.cycle2.min.js', array('scope' => 'header'));
  drupal_add_js(libraries_get_path('cycle2.swipe') . '/jquery.cycle2.swipe.min.js', array('scope' => 'header'));

  $format_info = $variables['format_info'];
  $module_path = drupal_get_path('module', $format_info['module']);
  $js_path = $module_path . '/' . $format_info['path'] . '/slideshow_thumbnails.js';

  drupal_add_js($js_path, array('scope' => 'header'));
}
