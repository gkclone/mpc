<div<?php print $attributes; ?>>
  <h2<?php print $title_attributes; ?>><?php print $title; ?></h2>
  <div class="PromotionGroup-progress"><div class="inner"></div></div>

  <?php if ($show_controls): ?>
  <div class="PromotionGroup-controls">
    <div class="PromotionGroup-prev Icon Icon--angleLeft"></div>
    <div class="PromotionGroup-pager"></div>
    <div class="PromotionGroup-next Icon Icon--angleRight"></div>
  </div>
  <?php endif; ?>

  <?php foreach ($promotions as $promotion): ?>
    <?php print render($promotion); ?>
  <?php endforeach; ?>
</div>
