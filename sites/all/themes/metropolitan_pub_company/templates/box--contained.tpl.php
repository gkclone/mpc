<div<?php print $container_attributes; ?>>
  <div class="Container-inner">
    <?php if ($content): ?>
      <?php if ($box_prefix): ?>
        <?php print $box_prefix; ?>
      <?php endif; ?>

      <div<?php print $attributes; ?>>
        <?php if ($admin_links): ?>
          <?php print $admin_links; ?>
        <?php endif; ?>

        <?php print render($title_prefix); ?>
        <?php if ($title): ?>
        <h2<?php print $title_attributes; ?>><?php print $title ?></h2>
        <?php endif;?>
        <?php print render($title_suffix); ?>

        <?php if ($feeds): ?>
          <div class="feed">
            <?php print $feeds; ?>
          </div>
        <?php endif; ?>

        <div<?php print $content_attributes; ?>>
          <?php print render($content) ?>
        </div>

        <?php if ($links): ?>
          <div class="links">
            <?php print $links; ?>
          </div>
        <?php endif; ?>

        <?php if ($more): ?>
          <div class="more-link">
            <?php print $more; ?>
          </div>
        <?php endif; ?>
      </div>

      <?php if ($box_suffix): ?>
        <?php print $box_suffix; ?>
      <?php endif; ?>
    <?php endif; ?>
  </div>
</div>