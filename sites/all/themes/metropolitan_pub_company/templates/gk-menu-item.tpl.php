<div<?php print $attributes; ?>>
  <?php if (!empty($price)): ?>
    <div class="GKMenuItem-price"><?php echo $price ?></div>
  <?php endif; ?>
  <h3 class="Box-title">
    <?php echo $title ?>
  </h3>

  <div<?php print $content_attributes; ?>>

    <?php if (!empty($description)): ?>
      <div class="GKMenuItem-description">
        <?php echo $description ?>
      </div>
    <?php endif; ?>

    <?php if (!empty($terms)): ?>
      <?php echo render($terms) ?>
    <?php endif; ?>
  </div>
</div>
