<div class="Container Container--box Container--paneContentHeader">
  <div class="Container-inner">
    <?php if ($messages): ?>
      <?php print $messages; ?>
    <?php endif; ?>

    <?php if ($title): ?>
    <h1<?php print $title_attributes; ?>><?php print $title; ?></h1>
    <?php endif; ?>

    <?php if ($tabs_rendered = render($tabs)): ?>
      <?php print $tabs_rendered; ?>
    <?php endif; ?>

    <?php if ($help): ?>
    <div id="help" class="grid">
      <?php print render($page['help']); ?>
    </div>
    <?php endif; ?>

    <?php if ($action_links): ?>
      <ul class="action-links list--inline">
        <?php print render($action_links); ?>
      </ul>
    <?php endif; ?>
  </div>
</div>