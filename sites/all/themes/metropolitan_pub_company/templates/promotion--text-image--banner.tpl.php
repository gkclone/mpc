<div<?php print $attributes; ?>>
  <h2<?php print $title_attributes; ?>><?php print $title; ?></h2>
  <div class="Promotion-content">
    <?php print $body; ?>
  </div>
  <?php if ($cta && $is_link): ?>
    <div class="Promotion-cta">
      <a<?php print $link_attributes; ?>>
       <?php print $cta; ?>
      </a>
    </div>
  <?php endif; ?>
</div>