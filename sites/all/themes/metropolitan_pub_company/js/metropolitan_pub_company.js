(function($) {
  'use strict';

  Drupal.behaviors.metropolitan_pub_company = {
    attach: function(context, settings) {
      $('a[href*=#]:not([href=#])').click(function() {
        if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
          var target = $(this.hash);

          if (!target.length) {
            target = $('[name=' + this.hash.slice(1) +']');
          }

          if (target.length) {
            $('body').animate({
              scrollTop: target.offset().top
            }, 700, function() {
              if (history.pushState) {
                history.pushState(null, null, '#' + target.attr('id'));
              }
            });

            return false;
          }
        }

        return true;
      });

      // Run the mpc styleguide js
      if (typeof mpcStyleguide === 'object') {
        mpcStyleguide.init($);
      };

      // Run JS from the styleguide.
      if (typeof styleguide === 'object') {
        styleguide.init($);
      };
    }
  }

})(jQuery);
