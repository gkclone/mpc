<div class="Grid Grid--spaceHorizontal">
  <div class="Grid-cell u-size2of5">
    <div class="SecretDj-image">
      <a href="<?php print $link; ?>">
        <?php print $image; ?>
      </a>
    </div>
  </div>
  <div class="Grid-cell u-size3of5">
    <div class="SecretDj-song">
      <?php print $song; ?>
    </div>
    <div class="SecretDj-artist">
      <?php print $artist; ?>
    </div>
  </div>
</div>
