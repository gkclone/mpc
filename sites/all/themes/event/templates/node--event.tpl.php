<?php if ($content): ?>
  <div<?php print $attributes; ?>>
    <div class="Grid Grid--space">
      <div class="Grid-cell u-xl-size1of2 u-lg-size1of2 u-ie-size1of2">
        <div class="Node-image">
          <?php print render($field_event_image); ?>
        </div>
      </div>
      <div class="Grid-cell u-xl-size1of2 u-lg-size1of2 u-ie-size1of2">
        <?php if ($display_title): ?>
          <h1 class="Node-title">
            <?php print $title; ?>
          </h1>
        <?php endif; ?>
        <div class="Node-date">
          <?php print render($field_event_date); ?>
        </div>
        <div<?php print $content_attributes; ?>>
          <?php print_r($body); ?>
        </div>
      </div>
    </div>
  </div>
<?php endif; ?>
