<?php /*

  This template should match the teaser template and is needed because
  events on the homepage use this template

*/ ?>
<?php if ($content): ?>
  <div<?php print $attributes; ?>>
    <?php if ($display_title): ?>
      <h2 class="Node-title">
        <?php print $title; ?>
      </h2>
    <?php endif; ?>
    <div class="Node-content">
      <?php print render($field_event_date); ?>
      <?php if (!empty($links)): ?>
        <div class="u-textRight">
          <?php print $links; ?>
        </div>
      <?php endif; ?>
    </div>
  </div>
<?php endif; ?>
