; GK Distro
includes[gk_distro] = http://code.gkdigital.co/gk_distro/v2.0.26/gk_distro.make

; MODULES ======================================================================

; Standard
projects[gk_domain][type] = module
projects[gk_domain][subdir] = standard
projects[gk_domain][download][type] = git
projects[gk_domain][download][url] = git@bitbucket.org:greeneking/gk-domain.git
projects[gk_domain][download][tag] = 7.x-1.3

projects[gk_events][type] = module
projects[gk_events][subdir] = standard
projects[gk_events][download][type] = git
projects[gk_events][download][url] = git@bitbucket.org:greeneking/gk-events.git
projects[gk_events][download][tag] = 7.x-2.7

projects[gk_locations][type] = module
projects[gk_locations][subdir] = standard
projects[gk_locations][download][type] = git
projects[gk_locations][download][url] = git@bitbucket.org:greeneking/gk-locations.git
projects[gk_locations][download][tag] = 7.x-2.7

projects[gk_matchpint][type] = module
projects[gk_matchpint][subdir] = standard
projects[gk_matchpint][download][type] = git
projects[gk_matchpint][download][url] = git@bitbucket.org:greeneking/gk-matchpint.git
projects[gk_matchpint][download][tag] = 7.x-1.0

projects[gk_members][type] = module
projects[gk_members][subdir] = standard
projects[gk_members][download][type] = git
projects[gk_members][download][url] = git@bitbucket.org:greeneking/gk-members.git
projects[gk_members][download][tag] = 7.x-2.1

projects[gk_menus][type] = module
projects[gk_menus][subdir] = standard
projects[gk_menus][download][type] = git
projects[gk_menus][download][url] = git@bitbucket.org:greeneking/gk-menus.git
projects[gk_menus][download][tag] = 7.x-2.8

projects[gk_promotions][type] = module
projects[gk_promotions][subdir] = standard
projects[gk_promotions][download][type] = git
projects[gk_promotions][download][url] = git@bitbucket.org:greeneking/gk-promotions.git
projects[gk_promotions][download][tag] = 7.x-2.7

projects[gk_rewards][type] = module
projects[gk_rewards][subdir] = standard
projects[gk_rewards][download][type] = git
projects[gk_rewards][download][url] = git@bitbucket.org:greeneking/gk-rewards.git
projects[gk_rewards][download][tag] = 7.x-2.3

projects[gk_secret_dj][type] = module
projects[gk_secret_dj][subdir] = standard
projects[gk_secret_dj][download][type] = git
projects[gk_secret_dj][download][url] = git@bitbucket.org:greeneking/gk-secret-dj.git
projects[gk_secret_dj][download][tag] = 7.x-1.0

projects[gk_table_booking][type] = module
projects[gk_table_booking][subdir] = standard
projects[gk_table_booking][download][type] = git
projects[gk_table_booking][download][url] = git@bitbucket.org:greeneking/gk-table-booking.git
projects[gk_table_booking][download][tag] = 7.x-1.1

projects[gk_tiers][type] = module
projects[gk_tiers][subdir] = standard
projects[gk_tiers][download][type] = git
projects[gk_tiers][download][url] = git@bitbucket.org:greeneking/gk-tiers.git
projects[gk_tiers][download][tag] = 7.x-1.3

projects[gk_webform][type] = module
projects[gk_webform][subdir] = standard
projects[gk_webform][download][type] = git
projects[gk_webform][download][url] = git@bitbucket.org:greeneking/gk-webform.git
projects[gk_webform][download][tag] = 7.x-2.2

; Contrib
projects[domain_menu_block][type] = module
projects[domain_menu_block][subdir] = contrib
projects[domain_menu_block][download][type] = get
projects[domain_menu_block][download][url] = http://ftp.drupal.org/files/projects/domain_menu_block-7.x-1.0-beta2.tar.gz
projects[domain_menu_block][download][tag] = 7.x-1.0-beta2

; LIBRARIES ====================================================================

projects[cycle2.carousel][type] = library
projects[cycle2.carousel][subdir] = ""
projects[cycle2.carousel][download][type] = file
projects[cycle2.carousel][download][url] = http://malsup.github.io/min/jquery.cycle2.carousel.min.js

projects[style_guide_metropolitan_pub_company][type] = library
projects[style_guide_metropolitan_pub_company][subdir] = ""
projects[style_guide_metropolitan_pub_company][download][type] = git
projects[style_guide_metropolitan_pub_company][download][url] = git@bitbucket.org:greeneking/mpc-style-guide-metropolitan-pub-company.git
projects[style_guide_metropolitan_pub_company][download][tag] = v1.0.6

projects[style_guide_city][type] = library
projects[style_guide_city][subdir] = ""
projects[style_guide_city][download][type] = git
projects[style_guide_city][download][url] = git@bitbucket.org:greeneking/mpc-style-guide-city.git
projects[style_guide_city][download][tag] = v1.0.1

projects[style_guide_country][type] = library
projects[style_guide_country][subdir] = ""
projects[style_guide_country][download][type] = git
projects[style_guide_country][download][url] = git@bitbucket.org:greeneking/mpc-style-guide-country.git
projects[style_guide_country][download][branch] = master

projects[style_guide_event][type] = library
projects[style_guide_event][subdir] = ""
projects[style_guide_event][download][type] = git
projects[style_guide_event][download][url] = git@bitbucket.org:greeneking/mpc-style-guide-event.git
projects[style_guide_event][download][branch] = master

projects[style_guide_food][type] = library
projects[style_guide_food][subdir] = ""
projects[style_guide_food][download][type] = git
projects[style_guide_food][download][url] = git@bitbucket.org:greeneking/mpc-style-guide-food.git
projects[style_guide_food][download][tag] = v1.1.7
